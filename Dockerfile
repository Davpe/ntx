FROM microsoft/dotnet:1.1.1-runtime
RUN apt update && apt install -y make git cpanminus && cpanm JSON::Tiny
WORKDIR /work
ADD release/ntx /run
ADD scripts/ntx /bin/ntx
RUN chmod a+x /bin/ntx
ENTRYPOINT ["dotnet", "/run/ntx.dll"]


