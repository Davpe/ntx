﻿using cz.ntx.proto.task;
using Google.Protobuf;
using Jose;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ntx.api.io;
using ntx.api.pipe;
using System;
using System.Collections.Async;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ntx.api.auth
{
    public static partial class Authorization
    {
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.api.auth");
        private class JsonContent : StringContent
        {
            public JsonContent(object obj) :
                base(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")
            { }
        }

        private class Auth0CredentialsGrantQuery
        {
            [JsonProperty("client_id")]
            public string ClientId { get; set; }
            [JsonProperty("grant_type")]
            public string GrantType { get; set; }
            [JsonProperty("scope")]
            public string Scope { get; set; }
            [JsonProperty("username")]
            public string Username { get; set; }
            [JsonProperty("password")]
            public string Password { get; set; }
            [JsonProperty("audience")]
            public string Audience { get; set; }
        }

        private class Auth0CredentialsGrantResponse
        {
            [JsonProperty("id_token")]
            public string IdToken { get; set; }
            [JsonProperty("access_token")]
            public string AccessToken { get; set; }
            [JsonProperty("refresh_token")]
            public string RefreshToken { get; set; }
            [JsonProperty("expires")]
            public string Expires { get; set; }
        }

        private class Auth0RefreshTokenGrantQuery
        {

            [JsonProperty("client_id")]
            public string ClientId { get; set; }
            [JsonProperty("grant_type")]
            public string GrantType { get; set; }
            [JsonProperty("refresh_token")]
            public string RefreshToken { get; set; }


            public string Encode()
            {
                return Convert.ToBase64String(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(this)));
            }
            public static Auth0RefreshTokenGrantQuery Decode(string s)
            {
                return JsonConvert.DeserializeObject<Auth0RefreshTokenGrantQuery>(Encoding.UTF8.GetString(Convert.FromBase64String(s)));
            }
        }
        private class Auth0RefreshTokenRevokeQuery
        {

            [JsonProperty("client_id")]
            public string ClientId { get; set; }
            [JsonProperty("token")]
            public string RefreshToken { get; set; }
        }

        class SingleOrArrayConverter<T> : JsonConverter
        {
            public override bool CanConvert(Type objectType)
            {
                return (objectType == typeof(List<T>));
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                JToken token = JToken.Load(reader);
                if (token.Type == JTokenType.Array)
                {
                    return token.ToObject<List<T>>();
                }
                return new List<T> { token.ToObject<T>() };
            }

            public override bool CanWrite
            {
                get { return false; }
            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                List<T> list = (List<T>)value;
                if (list.Count == 1)
                {
                    value = list[0];
                }
                serializer.Serialize(writer, value);
            }
        }

        private class Auth0JWT
        {
            [JsonProperty("iss")]
            public string Issuer { get; set; }
            [JsonProperty("exp")]
            public uint Expires { get; set; }
            [JsonProperty("aud")]
            [JsonConverter(typeof(SingleOrArrayConverter<string>))]
            public List<string> Audience { get; set; }
        }


        private static string GetDeviceId()
        {
            return "ntx-" + (Environment.GetEnvironmentVariable("NTX_DEVICE") ?? Environment.MachineName).ToLowerInvariant();
        }
        public static async Task Logout(Stream accessTokenStream, bool withRefresh)
        {
            
            string token = "";
            using (StreamReader r = new StreamReader(accessTokenStream))
            {
                token = r.ReadToEnd();
            }
            if (token.Contains("."))
                return;

            string temp = Path.GetTempPath();
            string file = Path.Combine(temp, token.Sha1Hash());
            _logger.LogInformation($"Deleting access token: {file}");
            File.Delete(file);

            if (!withRefresh)
                return;


            var decoded = Auth0RefreshTokenGrantQuery.Decode(token);


            _logger.LogInformation($"Revoking refresh token");
            using (var client = new HttpClient())
            {
                var query = new Auth0RefreshTokenRevokeQuery { ClientId = decoded.ClientId, RefreshToken = decoded.RefreshToken };
                var endpoint = decoded.GrantType.TrimEnd('/') + "/oauth/revoke";
                var resp = await client.PostAsync(endpoint,
                    new JsonContent(query));
                resp.EnsureSuccessStatusCode();
            }
        }

        public static async Task PrintToken (Func<Task<auth.Authorization.TaskAccessToken>> tokenFactory, Stream outputStream)
        {
            var token = await tokenFactory();

            using (StreamWriter w = new StreamWriter(outputStream, new UTF8Encoding(false)))
            {
                w.WriteLine($"Endpoint: {token.ServiceEndpoint}");
                w.WriteLine($"Store: {token.StoreEndpoint}");
                w.WriteLine($"Token: {token.Token}");
            }
        }

        private static async Task EnsureSuccess(this HttpResponseMessage resp)
        {

            if (resp.IsSuccessStatusCode)
                return;
            _logger.LogError(await resp.Content.ReadAsStringAsync());
            resp.EnsureSuccessStatusCode();
        }

        public static async Task<string> LoginWithPassword(string userName, string password, string clientId, string issuer, string audience, bool withRefresh)
        {
            using (var client = new HttpClient())
            {
                bool ret= client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", GetDeviceId());
                var query = new Auth0CredentialsGrantQuery
                {
                    Username = userName,
                    Password = password,
                    ClientId = clientId,
                    Audience = audience,
                    GrantType = "password",
                    Scope = withRefresh ? "offline_access" : ""
                };
                
                var endpoint = issuer.TrimEnd('/') + "/oauth/token";
                var resp = await client.PostAsync(endpoint,
                    new JsonContent(query));

                await resp.EnsureSuccess();
                
                var content = JsonConvert.DeserializeObject<Auth0CredentialsGrantResponse>(await resp.Content.ReadAsStringAsync());

                if (content.RefreshToken == null)
                    return content.AccessToken;


                var r = new Auth0RefreshTokenGrantQuery { ClientId = clientId, RefreshToken = content.RefreshToken, GrantType = issuer };
                return r.Encode();
            }

        }

        public class TaskAccessToken
        {
            string serviceEp="";
            public string Token { get; set; } = "";
            public string ServiceEndpoint { get => serviceEp; set => serviceEp=value.TrimEnd(); }
            public string StoreEndpoint => serviceEp + "/store";
            public string Role { get; set; } = "";
        }   
        private static string Sha1Hash(this string str)
        {
            
            using (var sha1 = MD5.Create())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(str));
                return Convert.ToBase64String(hash).Replace('/','_');
            }
        }

        private static string FetchAccessTokenFromCache(string refreshToken)
        {
            string temp = Path.GetTempPath();
            string file = Path.Combine(temp, refreshToken.Sha1Hash());
            var token = File.ReadAllText(file);
            return token;
        }

        private static DateTime unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private static uint ToUnixTime(this DateTime time)
        {
            return (uint)((time - unixEpoch).TotalSeconds);
        }

        private static DateTime FromUnixTime(uint unix)
        {
            return unixEpoch.Add(TimeSpan.FromSeconds(unix));
        }

        private static bool IsExpired(string token, TimeSpan offset)
        {
            var decoded =JWT.Payload<Auth0JWT>(token);
            var expires = FromUnixTime(decoded.Expires);
            _logger.LogInformation($"Access token expires: {expires.ToString("o")}");
            var now = DateTime.UtcNow.Add(offset);

            return now > expires;
        }
        private static async Task<string> FetchFromAuthService(string refreshToken)
        {
            _logger.LogInformation("Refreshing access token");
            var query = Auth0RefreshTokenGrantQuery.Decode(refreshToken);
            var rnd = new Random();
            var jitter = rnd.Next(0,1000);
            await Task.Delay(jitter);
            
            using (var client = new HttpClient())
            {
                bool ret = client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", GetDeviceId());
                var endpoint = query.GrantType.TrimEnd('/') + "/oauth/token";
                query.GrantType = "refresh_token";
                var resp = await client.PostAsync(endpoint,new JsonContent(query));

                await resp.EnsureSuccess();

                var content = JsonConvert.DeserializeObject<Auth0CredentialsGrantResponse>(await resp.Content.ReadAsStringAsync());

                try
                {
                    string temp = Path.GetTempPath();
                    string file = Path.Combine(temp, refreshToken.Sha1Hash());
                    File.WriteAllText(file, content.AccessToken);
                }
                catch { }
                return content.AccessToken;
            }
                
        }

        public static Func<Task<TaskAccessToken>> AccesTokenFactory(Stream stream)
        {
            string token = "";
            using (StreamReader r = new StreamReader(stream))
            {
                token =  r.ReadToEnd();
            }
                return () =>
            {
                return GetValidAccessToken(token);
            };
        }

        private static async Task<TaskAccessToken> GetValidAccessToken(string token)
        {
            
            if (!token.Contains("."))
            {
                try
                {
                    var accessToken = FetchAccessTokenFromCache(token);
                    if(IsExpired(accessToken, TimeSpan.FromMinutes(10)))
                    {
                        throw new Exception("Expired");
                    }
                    token = accessToken;
                }catch
                {
                    token = await FetchFromAuthService(token);
                }
            }
            var decoded = JWT.Payload<Auth0JWT>(token);
            var ret = new TaskAccessToken
            {
                Token = token,
                ServiceEndpoint = decoded.Audience.First( x => !x.StartsWith(decoded.Issuer))
            };
            return ret;
            
        }

        
    }

}
