﻿using cz.ntx.proto.auth;
using cz.ntx.proto.task;
using cz.ntx.proto.task.store;
using Google.Protobuf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;

namespace ntx.api.auth
{
    public static partial class Authorization
    {

        public static Func<System.Threading.Tasks.Task<NtxTokenResponse.Types.Token>> ToAuthorizedTaskFactory(this StoreContent.Types.Task task, Func<System.Threading.Tasks.Task<auth.Authorization.TaskAccessToken>> accesstokenFactory)
        {

            return async () =>

            {
                var token = await accesstokenFactory();
                using (var c = new HttpClient())
                {

                    var query = new NtxTokenRequest { Id = task.Profiles[0].Id, Label = task.Profiles[0].Labels[0], Role = token.Role };

                    c.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.Token);
                    var resp = await c.PostAsync(token.StoreEndpoint,
                        new StringContent(query.ToString(), Encoding.UTF8, "application/json"));
                    resp.EnsureSuccessStatusCode();
                    var content = await resp.Content.ReadAsStringAsync();
                    var ret = JsonParser.Default.Parse<NtxTokenResponse>(content).Token;
                    //ret.TokenType = "ntx-token";
                    return ret;
                }
            };
        }

        public static async System.Threading.Tasks.Task<TaskConfiguration> ParseFromStream(this TaskConfiguration task, Stream stream)
        {
            return   Google.Protobuf.JsonParser.Default.Parse<TaskConfiguration>(await new StreamReader(stream).ReadToEndAsync());
        }

        public static Func<System.Threading.Tasks.Task<NtxTokenResponse.Types.Token>> ToAuthorizedTaskFactory(this TaskConfiguration task, Func<System.Threading.Tasks.Task<auth.Authorization.TaskAccessToken>> accesstokenFactory, string label)
        {

            return async () =>

            {
                var token = await accesstokenFactory();
                using (var c = new HttpClient())
                {

                    var query = new CustomNtxTokenRequest {  Label= label, Role=token.Role, Task=task };

                    c.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.Token);
                    var resp = await c.PutAsync(token.StoreEndpoint,
                        new StringContent(query.ToString(), Encoding.UTF8, "application/json"));
                    resp.EnsureSuccessStatusCode();
                    var content = await resp.Content.ReadAsStringAsync();
                    var ret = JsonParser.Default.Parse<NtxTokenResponse>(content).Token;
                    //ret.TokenType = "ntx-token";
                    return ret;
                }
            };
        }


    }
}
