﻿using cz.ntx.proto.task;
using cz.ntx.proto.task.store;
using System;
using System.Collections.Generic;
using System.Linq;

using Async = System.Collections.Async;

namespace ntx.api.pipe.tasks
{
    public static partial class Extensions
    {
        public static Async.IAsyncEnumerable<StoreContent.Types.Task> ViaFilterAsync(this Async.IAsyncEnumerable<StoreContent.Types.Task> taskSource, Func<StoreContent.Types.Task, StoreContent.Types.Task> select)
        {
            return new Async.AsyncEnumerable<StoreContent.Types.Task>(async yield =>
            {
                var source = await taskSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    var ret = select(source.Current);
                    if (ret!=null)
                    {
                        await yield.ReturnAsync(ret);
                    }
                }
            }
            );
        }

        public static Async.IAsyncEnumerable<StoreContent.Types.Task> ViaFilterAsync(this Async.IAsyncEnumerable<StoreContent.Types.Task> taskSource, string serviceLabel,string taskLabel,string[] tags)
        {
            Func<StoreContent.Types.Task, StoreContent.Types.Task> serviceFunc;
            if (serviceLabel == null)
            {
                serviceFunc = x => x;
            }
            else
            {
                serviceFunc = (x) =>
                {
                    if(x.Service.Contains(serviceLabel))
                    {
                        return x;
                    }
                    else
                    {
                        return null;
                    }
                };
            }



            Func<StoreContent.Types.Task, StoreContent.Types.Task> taskFunc;
            if (taskLabel == null)
            { 
                taskFunc = x => x;
            }
            else
            {
                taskFunc = (x) => {
                    var ret = x.Clone();
                    ret.Profiles.Clear();
                    foreach (var p in x.Profiles)
                    {
                        if (p.Labels.Contains(taskLabel))
                        {
                            var pp = p.Clone();
                            ret.Profiles.Add(pp);
                            pp.Labels.Clear();
                            pp.Labels.Add(taskLabel);
                        }

                    }
                    return ret;
                };
            }
            Func<StoreContent.Types.Task, StoreContent.Types.Task> tagsFunc;
            
            if (tags == null)
            {
                tagsFunc = x => x;
            }
            else
            {
                //make it clear
                tagsFunc = (x) => {
                    foreach (var s in tags){
                        if (!x.Tags.Contains(s))
                            return null;
                    }
                    return x;
                };
            }
            return taskSource.ViaFilterAsync(serviceFunc).ViaFilterAsync(taskFunc).ViaFilterAsync(tagsFunc);
        }

        



    }
}
