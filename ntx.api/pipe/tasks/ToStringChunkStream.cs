﻿using cz.ntx.proto.task;
using cz.ntx.proto.task.store;
using System.Collections.Async;

namespace ntx.api.pipe.tasks
{
    public static partial class Extensions
    {
        public static System.Collections.Async.IAsyncEnumerable<string> ToStringChunkStream(this System.Collections.Async.IAsyncEnumerable<StoreContent.Types.Task> taskItemSource)
        {

            return new AsyncEnumerable<string>(async yield =>
            {
                var source = await taskItemSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    foreach (var v in source.Current.Profiles)
                    {
                        foreach (var l in v.Labels)
                        {
                            await yield.ReturnAsync($"{source.Current.Service} -l {l} -t {string.Join(" -t ", source.Current.Tags)}" + System.Environment.NewLine);
                        }
                    }
                }
            });
        }
    }
}
