﻿using cz.ntx.proto.v2t.engine;
using System;
using System.Threading.Tasks;
using System.Collections.Async;
using System.IO;
using System.Text;
using System.Threading;
using cz.ntx.proto.media;
using System.Text.RegularExpressions;
using System.Net.Http;

namespace ntx.api.pipe
{
    public static partial class PipeSource
    {
        public static IAsyncEnumerable<T> FromList<T>(System.Collections.Generic.List<T> list, CancellationToken token = default(CancellationToken)) where T : Google.Protobuf.IMessage, new()
        {
            return new AsyncEnumerable<T>(async yield =>
            {
                //token.Register(() => yield.Break());
                foreach (var i in list)
                {
                    
                    await yield.ReturnAsync(i);
                    if (token.IsCancellationRequested)
                        break;
                }
            });
        }

        public static IAsyncEnumerable<T> FromProtoJsonStream<T>(Stream stream, CancellationToken token) where T : Google.Protobuf.IMessage, new()
        {
            return new AsyncEnumerable<T>(async yield =>
            {
                StreamReader _stream = null;

                while (true)
                {
                    try
                    {
                        if (_stream == null)
                        {
                            _stream = new StreamReader(stream, new UTF8Encoding());
                        }
                        var line = await Task.Run(() => _stream.ReadLineAsync(), token);
                        if (line == null)
                            break;
                        var cancellationToken = yield.CancellationToken;
                        await yield.ReturnAsync(Google.Protobuf.JsonParser.Default.Parse<T>(line));
                    }
                    catch (Exception ex)
                    {
                        if (token.IsCancellationRequested)
                            break;
                        else
                            throw ex;
                    }
                }

            });
        }


        public static IAsyncEnumerable<T> FromProtoBinaryStream<T>(Stream stream, CancellationToken token) where T : Google.Protobuf.IMessage, new()
        {
            return new AsyncEnumerable<T>(async yield =>
            {
                Stream _stream = stream;

                while (true)
                {
                    try
                    {

                        var buffer = new byte[4];
                        var br = await _stream.ReadAsync(buffer, 0, 4);
                        if (br < 4)
                            break;
                        var size = BitConverter.ToInt32(buffer, 0);
                        var data = new byte[size];
                        br = await _stream.ReadAsync(data, 0, size);
                        if (br < size)
                            throw new Exception("Invalid stream");
                        var t = new T();
                        t.MergeFrom(new Google.Protobuf.CodedInputStream(data));
                        var cancellationToken = yield.CancellationToken;
                        await yield.ReturnAsync(t);
                    }
                    catch (Exception ex)
                    {
                        if (token.IsCancellationRequested)
                            break;
                        else
                            throw ex;
                    }
                }

            });
        }

        public static IAsyncEnumerable<Events> EventsFromBinaryStream(Stream stream,  int chunkSize = 4096, CancellationToken token = default(CancellationToken))
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                Stream _stream = null;
                while (true)
                {
                    try
                    {
                        if (_stream == null)
                        {
                            _stream = stream;
                        }
                        var buffer = new byte[chunkSize];
                        //var cancellationToken = yield.CancellationToken;
                        int br = await _stream.ReadAsync(buffer, 0, buffer.Length, token);
                        if (br < 1)
                            break;

                        var ev = new Events();
                        ev.Events_.Add(new Event { Audio = new Event.Types.Audio() { Body = Google.Protobuf.ByteString.CopyFrom(buffer, 0, br) } });
                        await yield.ReturnAsync(ev);
                    }
                    catch (Exception ex)
                    {
                        if (token.IsCancellationRequested)
                            break;
                        else
                            throw ex;
                    }
                }
            });
        }

        public static IAsyncEnumerable<MediaData> MediaDataFromBinaryStream(Stream stream, CancellationToken token, int chunkSize = 4096)
        {
            return new AsyncEnumerable<MediaData>(async yield =>
            {
                while (true)
                {
                    try
                    {
                        var buffer = new byte[chunkSize];
                        //var cancellationToken = yield.CancellationToken;
                        int br = await stream.ReadAsync(buffer, 0, buffer.Length, token);
                        if (br < 1)
                            break;

                        var ev = new MediaData { Payload = Google.Protobuf.ByteString.CopyFrom(buffer, 0, br), Timestamp = DateTime.UtcNow.ToTimeOffsetFraction() };
                        await yield.ReturnAsync(ev);
                    }
                    catch (Exception ex)
                    {
                        if (token.IsCancellationRequested)
                            break;
                        else
                            throw ex;
                    }
                }
            });
        }


        public static IAsyncEnumerable<cz.ntx.proto.task.store.StoreContent.Types.Task> TasksFromStream(Stream stream, CancellationToken token = default(CancellationToken))
        {
            return new AsyncEnumerable<cz.ntx.proto.task.store.StoreContent.Types.Task>(async yield =>
            {
                StreamReader _stream = new StreamReader(stream, new UTF8Encoding());
                var line = await Task.Run(async () => await _stream.ReadToEndAsync(), token);
                var content = cz.ntx.proto.task.store.StoreContent.Parser.ParseJson(line);
                foreach (var t in content.Tasks)
                {
                    var cancellationToken = yield.CancellationToken;
                    await yield.ReturnAsync(t);
                }
            });
        }



        public static IAsyncEnumerable<cz.ntx.proto.task.store.StoreContent.Types.Task> TasksFromLocalStore(Func<Task<auth.Authorization.TaskAccessToken>> tokenFactory, CancellationToken breaker = default(CancellationToken))
        {
            return new AsyncEnumerable<cz.ntx.proto.task.store.StoreContent.Types.Task>(async yield =>
            {
                var token = await tokenFactory();
                using (var httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token.Token);
                    StreamReader _stream = new StreamReader(await httpClient.GetStreamAsync(token.StoreEndpoint));
                    var line = await Task.Run(async () => await _stream.ReadToEndAsync(), breaker);
                    var content = cz.ntx.proto.task.store.StoreContent.Parser.ParseJson(line);
                    foreach (var t in content.Tasks)
                    {
                        var cancellationToken = yield.CancellationToken;
                        await yield.ReturnAsync(t);
                    }
                }
            });
        }

        public static IAsyncEnumerable<Events> EventsFromRawTextStream(Stream stream,CancellationToken token)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                using (var reader = new StreamReader(stream))
                {
                    string line = null;

                    while ((line = await reader.ReadLineAsync()) != null)
                    {
                        if (line.StartsWith("#"))
                            continue;
                        var events = new Events();
                        events.Events_.Add(new Event { Label = new Event.Types.Label { Item = line } });
                        if (events.Events_.Count > 0)
                        {
                            await yield.ReturnAsync(events);
                        }
                    }

                }
            });
        }
    }
}
