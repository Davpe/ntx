﻿using cz.ntx.proto.media;
using ntx.api.pipe.ismdata;
using ntx.api.pipe.events;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ntx.api.pipe
{
    public static partial class PipeSink
    {
        public static IAsyncSink<T> ProtoJsonStream<T>(Stream stream) where T : Google.Protobuf.IMessage, new()
        {
            return  new JsonProtoWriter<T>(stream);
        }
        public static IAsyncSink<T> ProtoBinaryStream<T>(Stream stream) where T : Google.Protobuf.IMessage, new()
        {
            return new BinaryProtoWriter<T>(stream);
        }

        public static IAsyncSink<string> StringChunkStream(Stream stream)
        {
            return new StringChunkWriter(stream);
        }

        public static IAsyncSink<byte[]> BinaryChunkStream(Stream stream)
        {
            return new BinaryChunkWriter(stream);
        }

        public static IAsyncSink<ISMData> DashTarStream(Stream stream)
        {
            return new DashTarWriter(stream);
        }
        public static IAsyncSink<cz.ntx.proto.v2t.engine.Events> ConsoleEvents()
        {
            return new ConsoleWriter();
        }
        public static IAsyncSink<T> NullSink<T>()
        {
            return new NullWriter<T>();
        }
    }
}
