﻿using cz.ntx.proto.v2t.engine;
using System.Collections.Async;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ntx.api.pipe.events
{
    public static partial class Extensions
    {
        public static System.Collections.Async.IAsyncEnumerable<string> ToSimpleLogFormat(this System.Collections.Async.IAsyncEnumerable<Events> eventsSource, bool withNoises, bool isLive)
        {
            return new AsyncEnumerable<string>(async yield =>
            {
                long index = 0;
                var source = await eventsSource.GetAsyncEnumeratorAsync();
                TimeSpan lastTime = TimeSpan.Zero;
                DateTime? start=null;
                var prob = 1.0;
                while (await source.MoveNextAsync())
                {
                    if (!start.HasValue)
                        start = DateTime.UtcNow;
                    var events = source.Current.Events_;
                    var startTime = TimeSpan.FromTicks((long)events.First(x => x.BodyCase == Event.BodyOneofCase.Timestamp).Timestamp.GetValue());
                    var endTime = TimeSpan.FromTicks((long)events.Last(x => x.BodyCase == Event.BodyOneofCase.Timestamp).Timestamp.GetValue());
                    lastTime = endTime;



                    var text = string.Join("", events.Where(x => x.BodyCase == Event.BodyOneofCase.Label).Select(x => x.Label.GetValue()));

                    bool isNoisy =
                        events.Count(x => x.BodyCase == Event.BodyOneofCase.Label) ==
                        events.Count(x => x.BodyCase == Event.BodyOneofCase.Label && x.Label.LabelCase == Event.Types.Label.LabelOneofCase.Noise);
                    if (isNoisy && !withNoises)
                    {
                        continue;
                    }
                    text = text.Trim();
                    
                    var cFound = events.FirstOrDefault(x => x.BodyCase == Event.BodyOneofCase.Meta && x.Meta.BodyCase == Event.Types.Meta.BodyOneofCase.Confidence);
                    if(cFound!=null)
                    {
                        prob = cFound.Meta.Confidence.Value;
                    }
                    var st = startTime.ToString();
                    if(isLive)
                    {
                        st = start.Value.Add(startTime).ToString("o");
                    }

                    text=text.Replace(" ", "_");
                    var line = $"{st}\t{(endTime-startTime).TotalMilliseconds}\t{(100 * prob).ToString("0.00")}\t{text}";
                    index++;
                    await yield.ReturnAsync(line + "\n");

                }
            });
        }
    }
}
