﻿using cz.ntx.proto.v2t.engine;
using cz.ntx.proto.v2t.misc;
using System;
using System.Collections.Async;
using System.Linq;
using System.Text.RegularExpressions;

namespace ntx.api.pipe.events
{
    public static partial class Extensions
    {
        public static IAsyncEnumerable<Events> ViaFilterAsync(this IAsyncEnumerable<Events> source, EventFilter filter)
        {
            switch (filter.FilterCase)
            {
                case EventFilter.FilterOneofCase.Noise:
                    return source.StringSourceNoiseFilterWithTimestamp(filter.Noise);
                case EventFilter.FilterOneofCase.None:
                    return source.NoneFilter();
                default:
                    throw new Exception("Invalid filter type: " + filter.FilterCase.ToString());

            }
        }
        
        public static IAsyncEnumerable<Events> ViaLookAheadFilter(this IAsyncEnumerable<Events> eventSource)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = await eventSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    if (source.Current == null)
                    {
                        await yield.ReturnAsync(null);
                        continue;
                    }
                    if(!source.Current.Lookahead)
                    {
                        await yield.ReturnAsync(source.Current.Clone());
                        continue;
                    }

                }
            }
            );
        }

        public static IAsyncEnumerable<Events> ViaEventFilter(this IAsyncEnumerable<Events> eventSource, Func<Event,Event> filter )
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = await eventSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    if (source.Current == null)
                    {
                        await yield.ReturnAsync(null);
                        continue;
                    }

                    var ret = source.Current.Clone();
                    ret.Events_.Clear();
                    foreach (var e in source.Current.Events_)
                    {
                        var r = filter(e);
                        if (r != null)
                            ret.Events_.Add(r);
                    }

                    await yield.ReturnAsync(ret);
                    continue;
                    

                }
            }
            );
        }

        public static IAsyncEnumerable<Events> ViaEventsLimiterByCount(this IAsyncEnumerable<Events> eventSource, int maxEvents)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = await eventSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    if (source.Current == null)
                    {
                        await yield.ReturnAsync(null);
                        continue;
                    }
                    var ev = source.Current;
                    var rest= ((ev.Events_.Count) % maxEvents) > 0 ? 1 : 0; ;
                    int noBatches = (ev.Events_.Count) / maxEvents + rest;
                    var src = ev.Events_.Clone();
                    ev.Events_.Clear();
                    int skip = 0;
                    for(int i=0;i<noBatches;i++)
                    {
                        var ret= ev.Clone();
                        ret.Events_.AddRange(src.Skip(skip).Take(maxEvents));
                        skip += maxEvents;
                        await yield.ReturnAsync(ret);

                    }

                }
            }
            );
        }

        public static IAsyncEnumerable<Events> ViaInterceptor(this IAsyncEnumerable<Events> eventSource, Func<Events,Events> intercept, bool removeNull=false)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = await eventSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    if (source.Current == null)
                    {
                        await yield.ReturnAsync(null);
                        continue;
                    }

                    var ret = intercept(source.Current.Clone());
                    if (ret == null && removeNull)
                        continue;

                    await yield.ReturnAsync(ret);
                    continue;
                    

                }
            }
            );
        }

        public static IAsyncEnumerable<Events> ViaReLabelAndSplit(this IAsyncEnumerable<Events> eventSource, Regex split, Regex plus)
        {
            Func<Events, Events> interceptor = (x)=>
            {
                var ret = x.Clone();
                ret.Events_.Clear();
                foreach(var v in x.Events_)
                {
                    if(v.BodyCase!= Event.BodyOneofCase.Label)
                    {
                        ret.Events_.Add(v);
                        continue;
                    }
                    if(v.Label.LabelCase!= Event.Types.Label.LabelOneofCase.Item &&
                    v.Label.LabelCase != Event.Types.Label.LabelOneofCase.Plus
                    )
                    {
                        ret.Events_.Add(v);
                        continue;
                    }

                    var line = v.Label.GetValue();
                    foreach (var s in split.Split(line))
                    {
                        if (s == "")
                            continue;
                        if (plus.IsMatch(s))
                        {
                            ret.Events_.Add(
                                new Event() { Label = new Event.Types.Label { Plus = s } }
                                );
                        }
                        else
                        {
                            ret.Events_.Add(
                                new Event() { Label = new Event.Types.Label { Item = s } }
                                );
                        }

                    }




                }
                if (ret.Events_.Count == 0)
                    return null;

                return ret;
            };

            return eventSource.ViaInterceptor(interceptor,true);
        }

        public static IAsyncEnumerable<Events> ViaTimestampFilter(this IAsyncEnumerable<Events> eventSource)
        {
            return new AsyncEnumerable<Events>(async yield => {
                var source = await eventSource.GetAsyncEnumeratorAsync();

                while (await source.MoveNextAsync())
                {
                    var ret = new Events() { Lookahead = source.Current.Lookahead };
                    foreach (var e in source.Current.Events_)
                    {
                        switch (e.BodyCase)
                        {
                            case Event.BodyOneofCase.Timestamp:
                                break;
                            default:
                                ret.Events_.Add(e);
                                break;
                        }
                    }
                    if (ret.Events_.Count > 0)
                        await yield.ReturnAsync(ret);
                }
            });
        }

        private static IAsyncEnumerable<Events> NoneFilter(this IAsyncEnumerable<Events> eventSource)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = await eventSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    var ret = source.Current.Clone();
                    await yield.ReturnAsync(ret);
                    
                }

            });
        }
        
        private static IAsyncEnumerable<Events> StringSourceNoiseFilterWithTimestamp(this IAsyncEnumerable<Events> eventSource, EventFilter.Types.NoiseEventFilter filter)
        {
            return new AsyncEnumerable<Events>(async yield => {
                var source = await eventSource.GetAsyncEnumeratorAsync();
                long speechHead = 0; long noiseHead = 0; bool canPrint = false;
                Event.Types.Label.LabelOneofCase lastSeen = Event.Types.Label.LabelOneofCase.None;

                Event timestampEvent = null;

                while (await source.MoveNextAsync())
                {
                    var ret = new Events() { Lookahead = source.Current.Lookahead };
                    foreach (var e in source.Current.Events_)
                    {
                        switch (e.BodyCase)
                        {
                            case Event.BodyOneofCase.Timestamp:
                                if (e.Timestamp.ValueCase == Event.Types.Timestamp.ValueOneofCase.Timestamp_)
                                {
                                    timestampEvent = e;
                                }
                                switch (lastSeen)
                                {
                                    case Event.Types.Label.LabelOneofCase.Noise:
                                        if ((long)e.Timestamp.Recovery > noiseHead)
                                            noiseHead = (long)e.Timestamp.Recovery;
                                        break;
                                    case Event.Types.Label.LabelOneofCase.Item:
                                    case Event.Types.Label.LabelOneofCase.Plus:
                                        if ((long)e.Timestamp.Recovery > speechHead)
                                            speechHead = (long)e.Timestamp.Recovery;
                                        break;
                                }
                                break;
                            case Event.BodyOneofCase.Label:
                                lastSeen = e.Label.LabelCase;
                                switch (e.Label.LabelCase)
                                {
                                    case Event.Types.Label.LabelOneofCase.Noise:
                                        if (canPrint && ((noiseHead - speechHead) > (long)filter.Duration))
                                        {
                                            canPrint = false;
                                            ret.Events_.Add(new Event() { Label = new Event.Types.Label { Noise = filter.ParagraphSymbol } });
                                        }
                                        break;
                                    case Event.Types.Label.LabelOneofCase.Item:
                                    case Event.Types.Label.LabelOneofCase.Plus:
                                        canPrint = true;
                                        ret.Events_.Add(e);
                                        if (timestampEvent != null)
                                        {
                                            ret.Events_.Add(timestampEvent.Clone());
                                            timestampEvent = null;
                                        }
                                        break;
                                }
                                break;
                        }
                    }
                    if (ret.Events_.Count > 0)
                        await yield.ReturnAsync(ret);
                }
            });
        }        
    }
}
