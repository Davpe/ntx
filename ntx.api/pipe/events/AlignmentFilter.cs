﻿using cz.ntx.proto.v2t.misc;
using System;
using System.Collections.Async;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ntx.api.pipe.events
{
    public static partial class Extensions
    {

        public static string GetValue(this cz.ntx.proto.v2t.engine.Event.Types.Label ev)
        {
            switch(ev.LabelCase)
            {
                case cz.ntx.proto.v2t.engine.Event.Types.Label.LabelOneofCase.Item:
                    return ev.Item;
                case cz.ntx.proto.v2t.engine.Event.Types.Label.LabelOneofCase.Noise:
                    return ev.Noise;
                case cz.ntx.proto.v2t.engine.Event.Types.Label.LabelOneofCase.Plus:
                    return ev.Plus;
                default:
                    throw new Exception($"Unsupported label: {ev.LabelCase}");
            }
        }
        
        public static ulong GetValue(this cz.ntx.proto.v2t.engine.Event.Types.Timestamp ev)
        {
            switch(ev.ValueCase)
            {
                case cz.ntx.proto.v2t.engine.Event.Types.Timestamp.ValueOneofCase.Recovery:
                    return ev.Recovery;
                case cz.ntx.proto.v2t.engine.Event.Types.Timestamp.ValueOneofCase.Timestamp_:
                    return ev.Timestamp_;
                default:
                    throw new Exception($"Unsupported timestamp: {ev.ValueCase}");
            }
        }

        public static async System.Threading.Tasks.Task<Score> ToEvaluationItem(this System.Collections.Async.IAsyncEnumerable<Alignment> alignSource, string itemId)
        {


            var score = new Score
            {
                Count = 0,
                Deletions = 0,
                Insertions = 0,
                Hits = 0,
                Substitutions = 0
            };



            var source = await alignSource.GetAsyncEnumeratorAsync();
            while (await source.MoveNextAsync())
            {
                var item = source.Current;
                switch (item.AlignmentCase)
                {
                    case Alignment.AlignmentOneofCase.AlignedLabel:
                        switch(item.AlignedLabel.AType)
                        {
                            case Alignment.Types.AlignType.Hit:

                                score.Count++;
                                score.Hits++;
                                break;
                            case Alignment.Types.AlignType.Ins:
                                score.Insertions++;
                                break;
                            case Alignment.Types.AlignType.Del:
                                score.Deletions++;
                                score.Count++;
                                break;
                            case Alignment.Types.AlignType.Sub:
                                score.Substitutions++;
                                score.Count++;
                                break;
                        }
                        break;
                    case Alignment.AlignmentOneofCase.AlignedTimeStamp:


                        break;
                }
            }
            return score;
        }
      

        public static System.Collections.Async.IAsyncEnumerable<Alignment> ViaEvalTypeMarkerAsync(
            this System.Collections.Async.IAsyncEnumerable<Alignment> alignSource,
            CancellationToken breaker = default(CancellationToken)
            )
        {
            return new AsyncEnumerable<Alignment>(async yield => {

                var source = await alignSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    if (source.Current == null)
                    {
                        continue;
                    }
                    
                    if(source.Current.AlignmentCase!= Alignment.AlignmentOneofCase.AlignedLabel)
                    {
                        await yield.ReturnAsync(source.Current);
                        continue;
                    }
                    var item = source.Current.AlignedLabel;
                    
                    if (item.Res == null)
                    {
                        if(item.Ref.LabelCase== cz.ntx.proto.v2t.engine.Event.Types.Label.LabelOneofCase.Item)
                            item.AType = Alignment.Types.AlignType.Del;
                        await yield.ReturnAsync(source.Current);
                        continue;
                    }
                    if(item.Ref==null)
                    {
                        if(item.Res.LabelCase== cz.ntx.proto.v2t.engine.Event.Types.Label.LabelOneofCase.Item)
                            item.AType = Alignment.Types.AlignType.Ins;
                        await yield.ReturnAsync(source.Current);
                        continue;
                    }
                    
                    if(item.Ref.LabelCase!= cz.ntx.proto.v2t.engine.Event.Types.Label.LabelOneofCase.Item)
                    {
                        await yield.ReturnAsync(source.Current);
                        continue;
                    }
                    //HIT or SUB
                    if (item.Ref.GetValue().ToLowerInvariant() == item.Res.GetValue().ToLowerInvariant())
                        item.AType = Alignment.Types.AlignType.Hit;
                    else
                        item.AType = Alignment.Types.AlignType.Sub;

                    await yield.ReturnAsync(source.Current);

                }
            });
        }
    }
}