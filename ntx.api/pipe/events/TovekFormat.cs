﻿using cz.ntx.proto.v2t.engine;
using System.Collections.Async;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ntx.api.pipe.events
{
    public static partial class Extensions
    {
        public static System.Collections.Async.IAsyncEnumerable<string> ToTovekFormat(this System.Collections.Async.IAsyncEnumerable<Events> eventsSource,long channelId ,string replaceNoises,string whiteSpaceSymbol, bool channelAdd)
        {
            return new AsyncEnumerable<string>(async yield =>
            {
                long index = 0;
                var source = await eventsSource.GetAsyncEnumeratorAsync();
                TimeSpan lastTime = TimeSpan.Zero;
                bool lastNoisy = true;
                while (await source.MoveNextAsync())
                {
                    var events = source.Current.Events_;
                    var startTime = TimeSpan.FromTicks((long)events.First(x => x.BodyCase == Event.BodyOneofCase.Timestamp).Timestamp.GetValue());
                    var endTime = TimeSpan.FromTicks((long)events.Last(x => x.BodyCase == Event.BodyOneofCase.Timestamp).Timestamp.GetValue());
                    lastTime = endTime;



                    var text = string.Join("", events.Where(x => x.BodyCase == Event.BodyOneofCase.Label).Select(x => x.Label.GetValue()));
                    
                    bool isNoisy =
                        events.Count(x => x.BodyCase == Event.BodyOneofCase.Label) ==
                        events.Count(x => x.BodyCase == Event.BodyOneofCase.Label && x.Label.LabelCase == Event.Types.Label.LabelOneofCase.Noise);
                    if (isNoisy && replaceNoises != "origin")
                    {
                        text = replaceNoises;
                    }
                    text = text.Trim();
                    if(whiteSpaceSymbol != "origin")
                    {
                        text=string.Join(whiteSpaceSymbol,Regex.Split(text, @"\s+"));
                    }

                    if (!isNoisy && lastNoisy)
                    {
                        var startLine = $"T={index} ST={startTime.TotalSeconds} ET={startTime.TotalSeconds} W=<s{channelId+1}> P=1";
                        if (channelAdd)
                        {
                            startLine = $"T={index} ST={startTime.TotalSeconds} ET={startTime.TotalSeconds} W=<s> P=1 C={channelId}";
                        }

                        lastNoisy = false;
                        await yield.ReturnAsync(startLine + "\n");
                        index++;
                    }
                    if (!lastNoisy && isNoisy)
                    {
                        var endLine = $"T={index} ST={startTime.TotalSeconds} ET={startTime.TotalSeconds} W=</s{channelId + 1}> P=1";
                        if (channelAdd)
                        {
                            endLine = $"T={index} ST={startTime.TotalSeconds} ET={startTime.TotalSeconds} W=</s> P=1 C={channelId}";
                        }
                        await yield.ReturnAsync(endLine + "\n");
                        lastNoisy = true;
                    }
                    var line = $"T={index} ST={startTime.TotalSeconds} ET={endTime.TotalSeconds} W={text} P=1";
                    if(channelAdd)
                    {
                        line += $" C={channelId}";
                    }
                    index++;
                    await yield.ReturnAsync(line + "\n");
                   
                }
                if (!lastNoisy)
                {
                    var endLine = $"T={index} ST={lastTime.TotalSeconds} ET={lastTime.TotalSeconds} W=</s{channelId + 1}> P=1";
                    if (channelAdd)
                    {
                        endLine = $"T={index} ST={lastTime.TotalSeconds} ET={lastTime.TotalSeconds} W=</s> P=1 C={channelId}";
                    }
                    await yield.ReturnAsync(endLine + "\n");
                    lastNoisy = true;
                }
            });
        }
    }
}
