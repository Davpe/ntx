﻿using cz.ntx.proto.v2t.engine;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Async;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace ntx.api.pipe.events
{
    public static partial class Extensions
    {



        public static System.Collections.Async.IAsyncEnumerable<Events> ViaV2TStreaming(this System.Collections.Async.IAsyncEnumerable<Events> audioSource, EngineService.EngineServiceClient client, 
             EngineContext context, Func<Task<cz.ntx.proto.auth.NtxTokenResponse.Types.Token>> authResolver = null, CancellationToken breaker = default(CancellationToken)){
            ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.v2t.engine.EngineService");
            var start = DateTime.UtcNow;
            var cs = new CancellationTokenSource();
            var registration=breaker.Register(() => cs.Cancel());
            _logger.LogInformation("Allocating resources");
            return new AsyncEnumerable<Events>(async yield => {
                cz.ntx.proto.auth.NtxTokenResponse.Types.Token authToken = null;
                if (authResolver != null)
                {
                    try
                    {
                        authToken = await authResolver();
                    }catch(Exception ex)
                    {
                        _logger.LogError($"Task authorization failed: {ex.Message}");
                        throw ex;
                    }
                }
                
                using (var call = client.StreamingRecognize(Pipe.CreateDefaultGrpcCallOptions(cs.Token, authToken)))
                {
                    
                    
                    
                    await call.RequestStream.WriteAsync(new EngineStream() { Start = new EngineContextStart() { Context = context } });
                    await call.ResponseStream.MoveNext();
                    var meta = await call.ResponseHeadersAsync;
                    foreach (var v in meta)
                    {
                        if (v.IsBinary)
                            continue;
                        _logger.LogInformation($"Task meta, {v.Key} = {v.Value}");
                    }
                    registration.Dispose();
                    var resp = call.ResponseStream.Current;

                    if (resp.PayloadCase != EngineStream.PayloadOneofCase.Start)
                        throw new Exception("start expected");
                    _logger.LogInformation("Task started, start time {0} s",(DateTime.UtcNow-start));
                    var source = await audioSource.GetAsyncEnumeratorAsync();
                    var startTime = DateTime.UtcNow;
                    bool done = false;
                    while (!done)
                    {
                        bool pulled = false;
                        _logger.LogTrace(">>>PULL");
                        await call.RequestStream.WriteAsync(new EngineStream() { Pull = new EventsPull() });

                        while (!pulled)
                        {
                            await call.ResponseStream.MoveNext();
                            var response = call.ResponseStream.Current;
                            switch (response.PayloadCase)
                            {
                                case EngineStream.PayloadOneofCase.End:
                                    _logger.LogTrace("<<<END");
                                    done = true;
                                    pulled = true;
                                    break;
                                case EngineStream.PayloadOneofCase.Push:
                                    {
                                        _logger.LogTrace("<<<PUSH");
                                        response.Push.Events.ReceivedAt= (UInt64)DateTime.UtcNow.Subtract(startTime).Ticks;
                                        await yield.ReturnAsync(response.Push.Events);
                                        pulled = true;
                                    }
                                    break;
                                case EngineStream.PayloadOneofCase.Pull:
                                    _logger.LogTrace("\t<<<PULL");
                                    if (await source.MoveNextAsync())
                                    {
                                        _logger.LogTrace("\t>>>PUSH");
                                        await call.RequestStream.WriteAsync(new EngineStream() { Push = new EventsPush() { Events = source.Current } });

                                    }
                                    else
                                    {
                                        _logger.LogTrace("\t>>>END");
                                        await call.RequestStream.WriteAsync(new EngineStream() { End = new EngineContextEnd() });

                                    }
                                    break;
                            }
                        }
                    }
                    _logger.LogTrace("Completing ...");
                    await call.RequestStream.CompleteAsync();
                    var ret = await call.ResponseStream.MoveNext();
                    _logger.LogTrace("Completing done with {0}", ret);
                    _logger.LogInformation("Completed");
                }
            });
        }
    }
}
