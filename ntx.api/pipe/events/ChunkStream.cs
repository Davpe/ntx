﻿using cz.ntx.proto.v2t.engine;
using System;
using System.Collections.Async;
using System.Collections.Generic;
using System.Text;

namespace ntx.api.pipe.events
{
    public static partial class Extensions
    {
        public static System.Collections.Async.IAsyncEnumerable<string> ToStringChunkStream(this System.Collections.Async.IAsyncEnumerable<Events> eventsSource)
        {

            return new AsyncEnumerable<string>(async yield =>
            {
                var source = await eventsSource.GetAsyncEnumeratorAsync();
                string prevOutput = "";
                bool prevLookahead = false;
                while (await source.MoveNextAsync())
                {
                    string output = "";
                    foreach (var e in source.Current.Events_)
                    {
                        if (e.BodyCase == Event.BodyOneofCase.Label)
                        {
                            switch (e.Label.LabelCase)
                            {
                                case Event.Types.Label.LabelOneofCase.Item:
                                    output += e.Label.Item;
                                    break;
                                case Event.Types.Label.LabelOneofCase.Plus:
                                    output += e.Label.Plus;
                                    break;
                                case Event.Types.Label.LabelOneofCase.Noise:
                                    output += e.Label.Noise;
                                    break;
                            }
                        } else if (e.BodyCase == Event.BodyOneofCase.Timestamp) {
                            output += "[" + e.Timestamp.Timestamp_.ToString() + "]";
                        } else
                        {
                            continue;
                        }
                    }
                    if (prevLookahead)
                    {
                        var delete = new String('\b', prevOutput.Length);
                        await yield.ReturnAsync(delete);
                    }
                    prevOutput = output;
                    prevLookahead = source.Current.Lookahead;
                    if (output.Length > 0)
                    {
                        await yield.ReturnAsync(output);
                    }
                }
            });
        }

        public static System.Collections.Async.IAsyncEnumerable<byte[]> ToByteChunkStream(this System.Collections.Async.IAsyncEnumerable<Events> audioSource)
        {

            return new AsyncEnumerable<byte[]>(async yield =>
            {
                var source = await audioSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    foreach (var e in source.Current.Events_)
                    {
                        if (e.BodyCase != Event.BodyOneofCase.Audio)
                            continue;

                        if(e.Audio.Body.Length>0)
                        {
                            await yield.ReturnAsync(e.Audio.Body.ToByteArray());
                        }
                        
                    }
                }
                
            });
        }

        public static System.Collections.Async.IAsyncEnumerable<Event> ToEventStream(this System.Collections.Async.IAsyncEnumerable<Events> eventsSource)
        {

            return new AsyncEnumerable<Event>(async yield =>
            {
                var source = await eventsSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    foreach (var e in source.Current.Events_)
                    {
                        await yield.ReturnAsync(e);
                    }

                }
            });
        }

        public static System.Collections.Async.IAsyncEnumerable<Events> ToEventsStream(this System.Collections.Async.IAsyncEnumerable<Event> eventSource)
        {

            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = await eventSource.GetAsyncEnumeratorAsync();
                var cEvents = new Events();
                while (await source.MoveNextAsync())
                {
                    var current = source.Current;

                    if (current==null)
                    {
                        await yield.ReturnAsync(cEvents);
                        continue;
                    }
                    cEvents.Events_.Add(current);

                }
                await yield.ReturnAsync(cEvents);
            });
        }

        public static System.Collections.Async.IAsyncEnumerable<Event> ViaInterceptor(this System.Collections.Async.IAsyncEnumerable<Event> eventsSource, Func<Event,Event> intercept)
        {

            
            return new AsyncEnumerable<Event>(async yield =>
            {
                var source = await eventsSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    var item = source.Current;
                    await yield.ReturnAsync(intercept(item.Clone()));
                }
            });
        }
        public static System.Collections.Async.IAsyncEnumerable<Events> ViaLabelMerge(this System.Collections.Async.IAsyncEnumerable<Events> eventsSource)
        {
            return new AsyncEnumerable<Events>(async yield =>
            {
                var source = await eventsSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    var item = source.Current;
                    var ret = item.Clone();
                    ret.Events_.Clear();
                    var merged = await item.Events_.ToAsyncEnumerable().ViaLabelMerge().ToListAsync();
                    ret.Events_.AddRange(merged);

                    await yield.ReturnAsync(ret);

                }
            });
        }

        public static System.Collections.Async.IAsyncEnumerable<Event> ViaLabelMerge(this System.Collections.Async.IAsyncEnumerable<Event> eventsSource)
        {

            return new AsyncEnumerable<Event>(async yield =>
            {
                var source = await eventsSource.GetAsyncEnumeratorAsync();
                List<Event> block = new List<Event>();
                Event lastItem = null;
                while (await source.MoveNextAsync())
                {
                    var item =source.Current; 

                    if(item.BodyCase!= Event.BodyOneofCase.Label )
                    {
                        block.Add(item);
                        continue;
                    }
                    if (lastItem == null)
                    {
                        foreach(var i in block)
                        {
                            await yield.ReturnAsync(i);
                        }
                        block.Clear();
                        lastItem = item;
                        continue;
                    }
                    if (lastItem.Label.LabelCase == item.Label.LabelCase)
                    {
                        bool canContinue=true;
                        switch (lastItem.Label.LabelCase)
                        {
                            case Event.Types.Label.LabelOneofCase.Item:
                                block.Clear();
                                lastItem.Label.Item = lastItem.Label.Item + item.Label.Item;
                                break;
                            case Event.Types.Label.LabelOneofCase.Plus:
                                block.Clear();
                                lastItem.Label.Plus = lastItem.Label.Plus + item.Label.Plus;
                                break;
                            case Event.Types.Label.LabelOneofCase.Noise:
                                if(lastItem.Label.Noise==item.Label.Noise)
                                {
                                    block.Clear();
                                }
                                else
                                {
                                    canContinue = false;
                                }
                                break;
                        }

                        if(canContinue)
                            continue;   
                    }

                    await yield.ReturnAsync(lastItem);
                    foreach (var i in block)
                    {
                        await yield.ReturnAsync(i);
                    }
                    block.Clear();
                    lastItem = item;
                }
                if (lastItem != null)
                    await yield.ReturnAsync(lastItem);
                foreach (var i in block)
                {
                    await yield.ReturnAsync(i);
                }
            });
        }

    }
}

