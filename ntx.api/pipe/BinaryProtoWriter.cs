﻿using Google.Protobuf;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ntx.api.pipe
{
    public class BinaryProtoWriter<T> : IAsyncSink<T> where T : Google.Protobuf.IMessage, new()
    {
        System.IO.Stream stream = null;

        public BinaryProtoWriter(Stream stream)
        {
            this.stream = stream;
        }

        public Task CompleteAsync()
        {
            return Task.CompletedTask;
        }

        public async Task WriteAsync(T message)
        {
            int size = message.CalculateSize();
            var bSize = BitConverter.GetBytes(size);
            await stream.WriteAsync(bSize, 0, bSize.Length);
            byte[] result = new byte[size];
            CodedOutputStream output = new CodedOutputStream(result);
            message.WriteTo(output);
            await stream.WriteAsync(result, 0, result.Length);
            await stream.FlushAsync();
        }
    }
}
