﻿using System;
using System.Threading;

namespace ntx.api.pipe
{
    public static partial class Pipe
    {
        
        internal static Grpc.Core.CallOptions CreateDefaultGrpcCallOptions(CancellationToken token, cz.ntx.proto.auth.NtxTokenResponse.Types.Token authToken)
        {
            Grpc.Core.Metadata meta = null;
            if (authToken!=null){
                meta = new Grpc.Core.Metadata
                {
                    { authToken.TokenType, authToken.Token_ }
                };
            }
            return new Grpc.Core.CallOptions(meta, null, token);
        }

        public static DateTime unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public static cz.ntx.proto.TimeOffsetFraction ToTimeOffsetFraction(this DateTime time)
        {

            var t = time.ToUniversalTime() - unixEpoch;
            return new cz.ntx.proto.TimeOffsetFraction { Seconds = (ulong)(t.Ticks / 10000000), Ticks = (uint)(t.Ticks % 10000000) };
        }


        public static DateTime ToDateTime(this cz.ntx.proto.TimeOffsetFraction time)
        {
            return new DateTime(unixEpoch.Ticks + (long)time.Seconds * 10000000 + time.Ticks, DateTimeKind.Utc);
        }

        public static TimeSpan ToTimeSpan(this cz.ntx.proto.TimeOffsetFraction time)
        {
            return new TimeSpan((long)time.Seconds * 10000000 + time.Ticks);
        }
        public static cz.ntx.proto.TimeOffsetFraction ToTimeOffsetFraction(this TimeSpan span)
        {
            return new cz.ntx.proto.TimeOffsetFraction { Seconds = (ulong)(span.Ticks / 10000000), Ticks = (uint)(span.Ticks % 10000000) };
        }
    }
}
