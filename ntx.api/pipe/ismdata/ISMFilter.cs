﻿using cz.ntx.proto.media;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Async;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ntx.api.pipe.ismdata
{
    public static partial class Extensions
    {
        private static ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.api.pipe.ismdata");
        public static System.Collections.Async.IAsyncEnumerable<ISMData> ViaInterceptor(this System.Collections.Async.IAsyncEnumerable<ISMData> ismpSource, Func<ISMData, ISMData> interceptor)
        {
            return new AsyncEnumerable<ISMData>(async yield =>
            {
                var source = await ismpSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    var ret = interceptor(source.Current);
                    if (ret == null)
                        continue;
                    await yield.ReturnAsync(ret);
                }
            });
        }



        private static bool StartWith(this cz.ntx.proto.stream.Period what, string stream, string period, string adaptation, string representation)
        {
            if (what.StreamId.StartsWith(stream) && what.Id.StartsWith(period))
            {
                var ada = what.Adaptations.FirstOrDefault(x => x.Id.StartsWith(adaptation));
                if (ada == null)
                {
                    return false;
                }
                var repre = ada.Representations.FirstOrDefault(x => x.Id.StartsWith(representation));
                if (repre == null)
                {
                    return false;
                }
                return true;
            }

            return false;
        }

        private static cz.ntx.proto.stream.Period CloneAndFilter(this cz.ntx.proto.stream.Period p,
            string adaptation, string representation)
        {
            var ret = p.Clone();
            ret.Adaptations.Clear();
            var ada=p.Adaptations.First(x => x.Id == adaptation).Clone();
            var repre = ada.Representations.First(x => x.Id == representation).Clone();
            ada.Representations.Clear();
            ada.Representations.Add(repre);
            ret.Adaptations.Add(ada);
            ret.Header = repre.Header;
            return ret;
        }

        public static System.Collections.Async.IAsyncEnumerable<ISMData> ViaSinglePeriodFilter(
            this System.Collections.Async.IAsyncEnumerable<ISMData> ismpSource,
            string stream,string period,string adaptation, string representation)
        {
            

            return new AsyncEnumerable<ISMData>(async yield =>
            {
                List<ISMData> _buffer = new List<ISMData>();
                bool periodSelectionDone = false;
                var source = await ismpSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    var chunk = source.Current;
                    switch( chunk.PayloadCase)
                    {
                        case ISMData.PayloadOneofCase.Stream:
                            if (!periodSelectionDone)
                            {
                                _buffer.Add(chunk); 
                            }else
                            {
                                if(chunk.Stream.Id==stream)
                                    await yield.ReturnAsync(chunk);
                            }
                            break;
                        case ISMData.PayloadOneofCase.Period:
                            if(!periodSelectionDone)
                            {
                                if (chunk.Period.StartWith(stream, period, adaptation, representation))
                                {
                                    
                                    stream = chunk.Period.StreamId;
                                    period = chunk.Period.Id;
                                    _logger.LogInformation($"Found matching period = {period}, stream={stream}");
                                    periodSelectionDone = true;
                                    var s = _buffer.FirstOrDefault(x => x.PayloadCase == ISMData.PayloadOneofCase.Stream && x.Stream.Id == stream);
                                    if (s != null)
                                        await yield.ReturnAsync(s);
                                    await yield.ReturnAsync(chunk);
                                }
                            }else
                            {
                                if(chunk.Period.StreamId==stream && chunk.Period.Id==period)
                                    await yield.ReturnAsync(chunk);
                                return;

                            }
                            break;
                        case ISMData.PayloadOneofCase.Chunks:

                            if(periodSelectionDone && 
                               chunk.Chunks.Selector.Stream==stream && 
                               chunk.Chunks.Selector.Period==period)
                            {
                                await yield.ReturnAsync(chunk);
                            }
                            break;
                    }
                }
            });
        }
    }
}
