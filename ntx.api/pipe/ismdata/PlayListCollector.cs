﻿using cz.ntx.proto.media;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Async;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ntx.api.pipe.ismdata
{
    public static partial class Extensions
    {
        public static System.Collections.Async.IAsyncEnumerable<ISMData> ViaPlayListCollector(this System.Collections.Async.IAsyncEnumerable<ISMData> ismpSource)
        {
            ILogger _logger = Logging.LoggerFactory.CreateLogger("ViaPlayListCollector");
            return new AsyncEnumerable<ISMData>(async yield =>
            {
                cz.ntx.proto.stream.PeriodList periods = new cz.ntx.proto.stream.PeriodList();
                var source = await ismpSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    var chunk = source.Current;
                    switch (chunk.PayloadCase)
                    {
                        case ISMData.PayloadOneofCase.Stream:
                            await yield.ReturnAsync(chunk);
                            break;
                        case ISMData.PayloadOneofCase.Period:
                            if (chunk.Period.Closed)
                            {
                                var p = periods.Periods.First(x => x.Id == chunk.Period.Id && x.StreamId == chunk.Period.StreamId);
                                periods.Periods.Remove(p);
                                foreach (var a in chunk.Period.Adaptations)
                                {
                                    var pa = p.Adaptations.FirstOrDefault(x => x.Id == a.Id);
                                    if (pa != null)
                                    {
                                        a.Timeline = pa.Timeline.Clone();
                                    }
                                }
                                chunk.Period.Duration = p.Duration;
                                await yield.ReturnAsync(chunk);
                            }
                            else
                            {
                                await yield.ReturnAsync(chunk);
                                var p = chunk.Period.Clone();
                                periods.Periods.Add(p);
                                foreach (var a in p.Adaptations) { a.Timeline = new cz.ntx.proto.stream.SegmentTimeline(); }
                            }
                            break;
                        case ISMData.PayloadOneofCase.Chunks:

                            await yield.ReturnAsync(chunk);
                            var period = periods.Periods.First(x => x.Id == chunk.Chunks.Selector.Period && x.StreamId == chunk.Chunks.Selector.Stream);
                            period.Duration = Math.Max(chunk.Chunks.Chunks.Max(x => { return x.Offset + x.Duration; }), period.Duration);
                            var adpatation = period.Adaptations.First(x => x.Id == chunk.Chunks.Selector.Adaptation);
                            var firstRepreId = adpatation.Representations.First().Id;
                            if (chunk.Chunks.Selector.Representation != firstRepreId)
                                break;

                            var timeline = adpatation.Timeline;
                            foreach (var c in chunk.Chunks.Chunks)
                            {
                                if (timeline.Chunks.Count == 0)
                                {
                                    timeline.Chunks.Add(new cz.ntx.proto.stream.ChunkMeta { Count = 0, Offset = c.Offset, Duration = c.Duration });
                                    continue;
                                }
                                if (timeline.Chunks.Last().Duration == c.Duration)
                                {
                                    timeline.Chunks.Last().Count++;
                                    continue;
                                }
                                timeline.Chunks.Add(new cz.ntx.proto.stream.ChunkMeta { Count = 0, Offset = c.Offset, Duration = c.Duration });
                            }
                            break;
                    }
                }
            });
        }
    }
}
