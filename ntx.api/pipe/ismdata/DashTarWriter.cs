﻿using cz.ntx.proto.media;
using cz.ntx.proto.stream;
using ICSharpCode.SharpZipLib.Tar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ntx.api.pipe.ismdata
{

    internal static class TarUtils
    {
        public static void AddString(this TarOutputStream stream, string path, string content)
        {
            using (var m = new System.IO.MemoryStream())
            {
                using (var c = new System.IO.StreamWriter(m, new UTF8Encoding(false)))
                {
                    c.Write(content);
                    c.Flush();

                    TarEntry entry = TarEntry.CreateTarEntry(path);
                    entry.Size = m.Length;
                    m.Seek(0, System.IO.SeekOrigin.Begin);
                    stream.PutNextEntry(entry);
                    m.CopyTo(stream);
                    stream.CloseEntry();
                }
            }
        }
    }

    public class DashTarWriter : IAsyncSink<ISMData>
    {
        TarOutputStream tarStream=null;
        System.IO.Stream streamResolver;
        private StreamList streams = new StreamList();
        private PeriodList periods = new PeriodList();
        public DashTarWriter(System.IO.Stream streamResolver)
        {
            this.streamResolver = streamResolver;
        }

        public  Task WriteAsync(ISMData item)
        {
            if (tarStream == null)
            {
                tarStream = new TarOutputStream(streamResolver);
            }
            switch(item.PayloadCase)
            {
                case ISMData.PayloadOneofCase.Stream:
                    var s = streams.Streams.FirstOrDefault(x => x.Id == item.Stream.Id);
                    if (s != null)
                        streams.Streams.Remove(s);
                    streams.Streams.Add(item.Stream);
                    break;
                case ISMData.PayloadOneofCase.Period:
                    var p = periods.Periods.FirstOrDefault(x => x.Id == item.Period.Id && x.StreamId==item.Period.StreamId);
                    if (p != null)
                        periods.Periods.Remove(p);
                    periods.Periods.Add(item.Period);
                    break;
                case ISMData.PayloadOneofCase.Chunks:
                    var chunks = item.Chunks;
                    string path = string.Format("streams/{0}/periods/{1}/adaptations/{2}/representations/{3}/chunks/", chunks.Selector.Stream, chunks.Selector.Period, chunks.Selector.Adaptation, chunks.Selector.Representation);
                    foreach (var c in chunks.Chunks)
                    {
                        TarEntry entry = TarEntry.CreateTarEntry(path + c.Offset.ToString() + ".mp4");
                        entry.Size = c.Body.Length;
                        tarStream.PutNextEntry(entry);
                        c.Body.WriteTo(tarStream);
                        tarStream.CloseEntry();
                    }
                    break;
            }
            return Task.CompletedTask;
        }
        public async Task CompleteAsync()
        {
            await Task.Run(() =>
            {
                //Add Caddyfile
                tarStream.AddString("Caddyfile",
                string.Join(Environment.NewLine,
                "#get caddy from https://caddyserver.com",
                "localhost:8080",
                "header / {", "Access-Control-Allow-Origin  *",
                "}"));

                //Add Streamindex
                tarStream.AddString("streams/index.json", streams.ToString());

                foreach (var stream in streams.Streams)
                {
                    var periodList = new cz.ntx.proto.stream.PeriodList();
                    periodList.Periods.AddRange(periods.Periods.Where(x => x.StreamId == stream.Id));
                    tarStream.AddString(string.Format("streams/{0}/periods/index.json", stream.Id), periodList.ToString());
                    //Add manifest
                    using (var m = new System.IO.MemoryStream())
                    {
                        var manifest = periodList.ToXml();
                        manifest.Save(new System.IO.StreamWriter(m, new UTF8Encoding(false)));
                        TarEntry entry = TarEntry.CreateTarEntry(string.Format("streams/{0}/manifest.mpd", stream.Id));
                        entry.Size = m.Length;
                        tarStream.PutNextEntry(entry);
                        m.Seek(0, System.IO.SeekOrigin.Begin);
                        m.CopyTo(tarStream);
                        tarStream.CloseEntry();
                    }
                    //Add headers
                    foreach (var p in periodList.Periods)
                    {

                        foreach (var a in p.Adaptations)
                        {
                            foreach (var r in a.Representations)
                            {
                                string path = string.Format("streams/{0}/periods/{1}/adaptations/{2}/representations/{3}/header.mp4", stream.Id, p.Id, a.Id, r.Id);
                                TarEntry entry = TarEntry.CreateTarEntry(path);
                                entry.Size = r.Header.Length;
                                tarStream.PutNextEntry(entry);
                                r.Header.WriteTo(tarStream);
                                tarStream.CloseEntry();
                            }
                        }
                    }
                }

                tarStream.Flush();
                tarStream.Finish();
                tarStream.Dispose();


            });
        }
    }
}
