﻿using cz.ntx.proto.media;
using System;
using System.Collections.Async;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ntx.api.pipe.ismdata
{
    public static partial class Extensions
    {


        public static System.Collections.Async.IAsyncEnumerable<byte[]> ViaMP4Chunker(
            this System.Collections.Async.IAsyncEnumerable<ISMData> ismpSource)
        {


            return new AsyncEnumerable<byte[]>(async yield =>
            {

                bool headerDone = false;
                var source = await ismpSource.GetAsyncEnumeratorAsync();
                while (await source.MoveNextAsync())
                {
                    var chunk = source.Current;
                    switch (chunk.PayloadCase)
                    {
                        case ISMData.PayloadOneofCase.Period:
                            if(!headerDone)
                            {
                                await yield.ReturnAsync(chunk.Period.Header.ToByteArray());
                                headerDone = true;
                            }
                            break;
                        case ISMData.PayloadOneofCase.Chunks:
                            var ms = new MemoryStream();
                            foreach(var c in chunk.Chunks.Chunks)
                            {
                                c.Body.WriteTo(ms);
                            }
                            await yield.ReturnAsync(ms.ToArray());
                            break;
                    }
                }
            });
        }
    }
}
