﻿using cz.ntx.proto.stream;
using System;
using System.Xml;
namespace ntx.api.pipe.ismdata
{
    public static class Manifest
    {
        public static XmlDocument ToXml(this PeriodList periodList)
        {

            var doc = new XmlDocument();
            doc.AppendChild(doc.CreateXmlDeclaration("1.0", "UTF-8", "yes"));
            XmlElement root = doc.CreateElement("MPD", "urn:mpeg:DASH:schema:MPD:2011");
            root.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            root.SetAttribute("xsi:schemaLocation", "urn:mpeg:dash:schema:mpd:2011 http://standards.iso.org/ittf/PubliclyAvailableStandards/MPEG-DASH_schema_files/DASH-MPD.xsd");
            root.SetAttribute("type", "static");
            root.SetAttribute("profiles", "urn:mpeg:dash:profile:isoff-on-demand:2011"); //urn:mpeg:dash:profile:isoff-live
            root.SetAttribute("xmlns", "urn:mpeg:DASH:schema:MPD:2011");
            root.SetAttribute("maxSegmentDuration", "PT2S");
            root.SetAttribute("minBufferTime", "PT8S");
            doc.AppendChild(root);
            ulong totalDuration = 0;
            foreach (var p in periodList.Periods)
            {
                totalDuration += p.Duration;
            }
            string pt = XmlConvert.ToString(new TimeSpan((long)totalDuration));
            root.SetAttribute("mediaPresentationDuration", pt);

            foreach (var p in periodList.Periods)
            {
                root.AppendChild(p.ToXml(doc));
            }




            return doc;
        }
        public static XmlElement ToXml(this Period period, XmlDocument doc)
        {
            var p = doc.CreateElement("Period");
            string pt = XmlConvert.ToString(new TimeSpan((long)period.Duration));
            p.SetAttribute("duration", pt);
            p.SetAttribute("id", period.Id);
            foreach (var a in period.Adaptations)
            {
                switch (a.MimeType.ToLower())
                {
                    case "audio/mp4":
                        p.AppendChild(a.ToAudioXml(doc, period.Id));
                        break;
                    case "video/mp4":
                        p.AppendChild(a.ToVideoXml(doc, period.Id));
                        break;
                }
            }
            return p;
        }
        public static XmlElement ToVideoXml(this Adaptation adaptation, XmlDocument doc, string periodId)
        {
            var a = doc.CreateElement("AdaptationSet");
            a.SetAttribute("mimeType", adaptation.MimeType);
            a.SetAttribute("segmentAlignment", adaptation.SegmentAlignment.ToString().ToLower());
            a.SetAttribute("startWithSAP", "1"); //TODO
            a.SetAttribute("id", adaptation.Id);
            var template = doc.CreateElement("SegmentTemplate");
            template.SetAttribute("timescale", "10000000");
            template.SetAttribute("initialization", string.Format("periods/{0}/adaptations/{1}/representations/$RepresentationID$/header.mp4", periodId, adaptation.Id));
            template.SetAttribute("media", string.Format("periods/{0}/adaptations/{1}/representations/$RepresentationID$/chunks/$Time$.mp4", periodId, adaptation.Id));
            a.AppendChild(template);
            template.AppendChild(adaptation.Timeline.ToXml(doc));
            foreach (var r in adaptation.Representations) { a.AppendChild(r.ToVideoXml(doc)); }
            return a;
        }
        public static XmlElement ToAudioXml(this Adaptation adaptation, XmlDocument doc, string periodId)
        {
            var a = doc.CreateElement("AdaptationSet");
            a.SetAttribute("mimeType", adaptation.MimeType);
            a.SetAttribute("segmentAlignment", adaptation.SegmentAlignment.ToString().ToLower());
            a.SetAttribute("startWithSAP", "1"); //TODO
            a.SetAttribute("id", adaptation.Id);
            a.SetAttribute("lang", adaptation.Lang);
            var template = doc.CreateElement("SegmentTemplate");
            template.SetAttribute("timescale", "10000000");
            template.SetAttribute("initialization", string.Format("periods/{0}/adaptations/{1}/representations/$RepresentationID$/header.mp4", periodId, adaptation.Id));
            template.SetAttribute("media", string.Format("periods/{0}/adaptations/{1}/representations/$RepresentationID$/chunks/$Time$.mp4", periodId, adaptation.Id));

            a.AppendChild(template);
            template.AppendChild(adaptation.Timeline.ToXml(doc));
            foreach (var r in adaptation.Representations) { a.AppendChild(r.ToAudioXml(doc)); }
            return a;
        }
        public static XmlElement ToAudioXml(this Representation repre, XmlDocument doc)
        {
            var r = doc.CreateElement("Representation");
            r.SetAttribute("id", repre.Id);
            r.SetAttribute("codecs", repre.Codecs);
            r.SetAttribute("audioSamplingRate", repre.AudioSamplingRate.ToString());
            r.SetAttribute("bandwidth", repre.Bandwidth.ToString());
            return r;
        }
        public static XmlElement ToVideoXml(this Representation repre, XmlDocument doc)
        {
            var r = doc.CreateElement("Representation");
            r.SetAttribute("id", repre.Id);
            r.SetAttribute("codecs", repre.Codecs);
            r.SetAttribute("width", repre.Width.ToString());
            r.SetAttribute("height", repre.Height.ToString());
            r.SetAttribute("frameRate", repre.Framerate.ToString());
            r.SetAttribute("bandwidth", repre.Bandwidth.ToString());
            return r;   
        }
        public static XmlElement ToXml(this SegmentTimeline timeline, XmlDocument doc)
        {
            var t = doc.CreateElement("SegmentTimeline");

            foreach (var c in timeline.Chunks)
            {
                var s = doc.CreateElement("S");
                s.SetAttribute("t", c.Offset.ToString());
                s.SetAttribute("d", c.Duration.ToString());
                s.SetAttribute("r", c.Count.ToString());
                t.AppendChild(s);
            }
            return t;
        }
    }
}
