﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace ntx.api.pipe
{
    public class BinaryChunkWriter : IAsyncSink<byte[]>
    {
        Stream stream = null;
        Stream streamResolver;
        public BinaryChunkWriter(Stream streamResolver)
        {
            this.streamResolver = streamResolver;
        }

        public Task CompleteAsync()
        {
            return Task.CompletedTask;
        }

        public async Task WriteAsync(byte[] chunk)
        {
            if (stream == null)
            {
                stream = streamResolver;
            }

            await stream.WriteAsync(chunk,0,chunk.Length);
            await stream.FlushAsync();
        }
    }
}
