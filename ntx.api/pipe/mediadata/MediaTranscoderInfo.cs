﻿using System;
using System.Collections.Async;
using System.Collections.Generic;
using cz.ntx.proto.media;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Linq;

namespace ntx.api.pipe.mediadata
{
    public static partial class MediaTranscoder
    {

        public static async Task<MediaSource> GetMediaInfo(this System.Collections.Async.IAsyncEnumerable<MediaData> ismSource, MediaService.MediaServiceClient client,
          MediaStart config, Task<cz.ntx.proto.auth.NtxTokenResponse.Types.Token> authResolver = null)
        {
            MediaSource source = null;
            var sink = PipeSink.NullSink<ISMData>();
            await ismSource.ViaTranscoder(client, config, (s) => { source = s; return null; }, authResolver).RunWithSink(sink);
            return source;
        }
    }
}
