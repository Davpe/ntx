﻿using System;
using System.Collections.Async;
using System.Collections.Generic;
using cz.ntx.proto.media;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Linq;
using ntx.api.pipe;
using ntx.api.util;

namespace ntx.api.pipe.mediadata
{
    public static partial class MediaTranscoder
    {
        
        public static System.Collections.Async.IAsyncEnumerable<ISMData> ViaTranscoder(this System.Collections.Async.IAsyncEnumerable<MediaData> ismSource, MediaService.MediaServiceClient client,
           MediaStart config ,Func<MediaSource, MediaMapping> mapper, Task<cz.ntx.proto.auth.NtxTokenResponse.Types.Token> authResolver = null)
        {
            ILogger _logger = Logging.LoggerFactory.CreateLogger("ntx.media.MediaService");
            var cs = new CancellationTokenSource();
            var start = DateTime.UtcNow;
            _logger.LogInformation("Allocating resources");
            return new System.Collections.Async.AsyncEnumerable<ISMData>(async yield =>
            {
                cz.ntx.proto.auth.NtxTokenResponse.Types.Token authToken = null;
                if (authResolver != null)
                {
                    authToken = await authResolver;
                }

                using (var call = client.StreamingTranscode(Pipe.CreateDefaultGrpcCallOptions(cs.Token, authToken)))
                {
                    cz.ntx.proto.stream.Stream currentStream = null;
                    var source = await ismSource.GetAsyncEnumeratorAsync();
                    bool eof = false;
                    while (!eof)
                    {
                        await call.RequestStream.WriteAsync(new MediaFlow { Start = config });
                        await call.ResponseStream.MoveNext();
                        var resp = call.ResponseStream.Current;
                        if (resp.PayloadCase != MediaFlow.PayloadOneofCase.Start)
                            throw new Exception("start expected");
                        _logger.LogInformation("Task started, start time {0} s", (DateTime.UtcNow - start));
                        bool done = false;
                        while (!done)
                        {
                            bool pushed = false;
                            _logger.LogTrace(">>>PULL");
                            await call.RequestStream.WriteAsync(new MediaFlow() { Pull = new MediaPull() });

                            while (!pushed)
                            {
                                await call.ResponseStream.MoveNext();
                                var response = call.ResponseStream.Current;
                                switch (response.PayloadCase)
                                {
                                    case MediaFlow.PayloadOneofCase.End:
                                        _logger.LogTrace("<<<END");
                                        done = true;
                                        pushed = true;
                                        break;
                                    case MediaFlow.PayloadOneofCase.Push:
                                        {
                                            _logger.LogTrace("<<<PUSH ");
                                            if (response.Push.PayloadCase == MediaPush.PayloadOneofCase.Ism)
                                            {
                                                var chunk = response.Push.Ism;
                                                if(chunk.PayloadCase == ISMData.PayloadOneofCase.Period)
                                                {
                                                    var p = chunk.Period;
                                                    if(p.Closed)
                                                        _logger.LogInformation($"Period end: id={p.Id} offset={p.Offset.ToDateTime().ToString("o")}");
                                                    else
                                                        _logger.LogInformation($"Period start: id={p.Id} offset={p.Offset.ToDateTime().ToString("o")}");
                                                }

                                                await yield.ReturnAsync(response.Push.Ism);
                                            }
                                            pushed = true;
                                        }
                                        break;
                                    case MediaFlow.PayloadOneofCase.Pull:
                                        _logger.LogTrace("\t<<<PULL ({0})", response.Pull.PayloadCase);
                                        switch (response.Pull.PayloadCase)
                                        {
                                            case MediaPull.PayloadOneofCase.Job:
                                                var job = response.Pull.Job.Clone();
                                                job.MediaMapping = mapper(job.MediaSource);
                                                //stop executing flow if MediaMapping is null
                                                if(job.MediaMapping ==null)
                                                {
                                                    job.MediaMapping = Utils.GetDefaultMediaMapper(Guid.NewGuid(),".*", ".*", ".*", true, false, 128000, 512000)(job.MediaSource);
                                                    eof = true;
                                                }
                                                if (currentStream == null)
                                                {
                                                    
                                                    currentStream = new cz.ntx.proto.stream.Stream()
                                                    {
                                                        Id = job.MediaMapping.StreamId,
                                                        Created = DateTime.UtcNow.ToString("o"),
                                                        Name = job.MediaSource.Programs.First(x => x.Id == job.MediaMapping.SrcProgramId).ServiceName
                                                    };
                                                    _logger.LogInformation($"Creating stream: id={currentStream.Id} name={currentStream.Name}");
                                                    await yield.ReturnAsync(new ISMData() { Stream = currentStream });
                                                }
                                                _logger.LogTrace("\t>>>PUSH");
                                                await call.RequestStream.WriteAsync(new MediaFlow() { Push = new MediaPush() { Job = job } });
                                                break;
                                            case MediaPull.PayloadOneofCase.Data:
                                                if (!eof && await source.MoveNextAsync())
                                                {
                                                    _logger.LogTrace("\t>>>PUSH");
                                                    await call.RequestStream.WriteAsync(new MediaFlow() { Push = new MediaPush() { Data = source.Current } });

                                                }
                                                else
                                                {
                                                    _logger.LogTrace("\t>>>END");
                                                    eof = true;
                                                    await call.RequestStream.WriteAsync(new MediaFlow() { End = new MediaEnd() });

                                                }
                                                break;
                                            default:
                                                _logger.LogWarning("Invalid flow {0}", response.Pull.PayloadCase);
                                                throw new Exception("Invalid flow " + response.Pull.PayloadCase.ToString());
                                        }
                                        break;
                                }
                            }
                        }
                    }
                    _logger.LogTrace("Completing ...");
                    await call.RequestStream.CompleteAsync();
                    currentStream.Closed = true;
                    _logger.LogInformation($"Closing stream: id={currentStream.Id} name={currentStream.Name}");
                    await yield.ReturnAsync(new ISMData() { Stream = currentStream });
                    var ret = await call.ResponseStream.MoveNext();
                    _logger.LogTrace("Completing done with {0}", ret);
                }
            });
        }
    }
}
