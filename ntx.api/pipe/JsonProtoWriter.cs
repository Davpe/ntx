﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace ntx.api.pipe
{
    public class JsonProtoWriter<T> : IAsyncSink<T> where T : Google.Protobuf.IMessage, new()
    {
        StreamWriter stream=null;

        public JsonProtoWriter(Stream stream)
        {
            this.stream = new StreamWriter(stream, new UTF8Encoding(false))
            {
                AutoFlush = true
            };
        }

        public async Task WriteAsync(T message)
        {
            await stream.WriteLineAsync(message.ToString());
            await stream.FlushAsync();
        }
        public Task CompleteAsync()
        {
            return Task.CompletedTask;
        }
    }
}
