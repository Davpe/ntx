﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ntx.api
{
    public static class Constants
    {
        public const string DefaultTextSplit = @"(?<=[\s>\.\,\:\?\!\-]+)|(?=[\s<\.\,\:\?\!\-]+)";
        public const string DefaultPlusMatch = @"^[\.\,\:\?\!\s\-]+$";
    }
}
