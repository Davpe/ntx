﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.IO;
using ntx.api.io;
using System.Text.RegularExpressions;
using cz.ntx.proto.media;
using System.Linq;
using System.Threading.Tasks;
using cz.ntx.proto.v2t.engine;
using System.Threading;
using System.Text;

namespace ntx.api.util
{
    public static class Utils
    {

        public static string ReadLineMasked(char mask = '*')
        {
            var sb = new StringBuilder();
            ConsoleKeyInfo keyInfo;
            while ((keyInfo = Console.ReadKey(true)).Key != ConsoleKey.Enter)
            {
                if (!char.IsControl(keyInfo.KeyChar))
                {
                    sb.Append(keyInfo.KeyChar);
                    Console.Write(mask);
                }
                else if (keyInfo.Key == ConsoleKey.Backspace && sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);

                    if (Console.CursorLeft == 0)
                    {
                        Console.SetCursorPosition(Console.BufferWidth - 1, Console.CursorTop - 1);
                        Console.Write(' ');
                        Console.SetCursorPosition(Console.BufferWidth - 1, Console.CursorTop - 1);
                    }
                    else Console.Write("\b \b");
                }
            }
            return sb.ToString();
        }

        public static string ExpandEnvironment(this string text)
        {
           return Regex.Replace(
                    text,
                    @"(\${(\S+?)})",
                    new MatchEvaluator(
                       m =>
                       {
                           string match = m.Groups[2].Value;
                           var r = System.Environment.GetEnvironmentVariable(match);
                           if (r == null)
                               throw new ArgumentException("Can't find: " + match);
                           return r;
                       }
                    )
                 );
        }
        public static void SetEnvironment(Stream stream)
        {
            using (var r = new StreamReader(stream))
            {
                string line = null;
                while ((line = r.ReadLine()) != null)
                {
                    line = line.Trim();
                    if (line.StartsWith("#") || !line.Contains("="))
                        continue;

                    var split = line.Split(new char[] { '=' }, 2);
                    if (split.Length != 2)
                        continue;
                    var key = split[0].Trim(); var value = split[1].Trim();
                    if (Environment.GetEnvironmentVariable(key) == null)
                    {
                        Environment.SetEnvironmentVariable(key, value);
                    }

                }
            }
        }
        public static Channel ToGRPCChannel(this Uri uri, string certificate = null)
        {
            Channel channel = null;
            string crt = null;
            if (certificate != null)
            {
                //_logger.Info("Using certificate: {0}", options.Certificate);
                crt = new StreamReader(StreamProvider.Input(certificate)().Result).ReadToEnd();
            }
            switch (uri.Scheme)
            {
                case "http":
                    channel = new Channel(string.Format("{0}:{1}", uri.Host, uri.Port < 1 ? 80 : uri.Port), ChannelCredentials.Insecure);
                    break;
                case "https":
                    channel = crt == null ? new Channel(uri.Host, uri.Port < 1 ? 443 : uri.Port, new SslCredentials()) : new Channel(uri.Host, uri.Port < 1 ? 443 : uri.Port, new SslCredentials(crt));
                    break;
                default:
                    throw new Exception("Invalid scheme: " + uri.Scheme);
            }
            return channel;
        }

        public static async Task<cz.ntx.proto.HealthCheckResponse> HealhCheck(this Channel channel,string service)
        {
            var client = new cz.ntx.proto.Health.HealthClient(channel);
            return await client.CheckAsync(new cz.ntx.proto.HealthCheckRequest { Service = service });

        }

        public static async Task AddLexiconFromUri(this cz.ntx.proto.v2t.engine.EngineContext context, string uri, CancellationToken breaker= default(CancellationToken))
        {
            if (uri == "none")
                return;
            if (context.ConfigCase != EngineContext.ConfigOneofCase.V2T)
                throw new Exception($"Can't set lexicon to {context.ConfigCase} module");

            var lexiconString = await new StreamReader(
                   LazyStream.Input(uri)
                    ).ReadToEndAsync();

            context.V2T.WithLexicon = Google.Protobuf.JsonParser.Default.Parse<Lexicon>(lexiconString);
        }

        


        public static cz.ntx.proto.v2t.engine.AudioFormat ParseCmd(this cz.ntx.proto.v2t.engine.AudioFormat audioEncoding, string cmd)
        {
            if (cmd.StartsWith("auto"))
            {
                uint probeSize = uint.Parse(cmd.Substring(5));
                audioEncoding.Auto = new cz.ntx.proto.v2t.engine.AudioFormat.Types.AutoDetect { ProbeSizeBytes = probeSize };
                return audioEncoding;
            }
            if (cmd=="ismp")
            {
                audioEncoding.Header = new cz.ntx.proto.v2t.engine.AudioFormat.Types.Header();
                return audioEncoding;
            }
            if (cmd.StartsWith("pcm"))
            {
                string[] s = cmd.Split(':');
                if (s.Length < 4)
                    throw new Exception("Invalid pcm definition: " + cmd);


                var sampleFormat = (cz.ntx.proto.v2t.engine.AudioFormat.Types.SampleFormat)Enum.Parse(typeof(cz.ntx.proto.v2t.engine.AudioFormat.Types.SampleFormat), string.Format("AudioSampleFormat{0}", s[1]), true);
                var sampleRate = (cz.ntx.proto.v2t.engine.AudioFormat.Types.SampleRate)Enum.Parse(typeof(cz.ntx.proto.v2t.engine.AudioFormat.Types.SampleRate), string.Format("AudioSampleRate{0}", s[2]), true);
                var channelLayout = (cz.ntx.proto.v2t.engine.AudioFormat.Types.ChannelLayout)Enum.Parse(typeof(cz.ntx.proto.v2t.engine.AudioFormat.Types.ChannelLayout), string.Format("AudioChannelLayout{0}", s[3]), true);
                audioEncoding.Pcm = new cz.ntx.proto.v2t.engine.AudioFormat.Types.PCM() { SampleFormat = sampleFormat, SampleRate = sampleRate, ChannelLayout = channelLayout };
                return audioEncoding;
            }
            throw new Exception("Invalid audio format: " + cmd);

        }
        public static int GetSampleRate(this AudioFormat.Types.PCM pcm)
        {
            switch (pcm.SampleRate)
            {
                case AudioFormat.Types.SampleRate.AudioSampleRate8000:
                    return 8000;
                case AudioFormat.Types.SampleRate.AudioSampleRate11025:
                    return 11025;
                case AudioFormat.Types.SampleRate.AudioSampleRate16000:
                    return 16000;
                case AudioFormat.Types.SampleRate.AudioSampleRate22050:
                    return 22050;

                case AudioFormat.Types.SampleRate.AudioSampleRate32000:
                    return 32000;
                    
                case AudioFormat.Types.SampleRate.AudioSampleRate44100:
                    return 44100;
                    
                case AudioFormat.Types.SampleRate.AudioSampleRate48000:
                    return 48000;
                    
                case AudioFormat.Types.SampleRate.AudioSampleRate96000:
                    return 96000;
                    
            }
            throw new Exception($"Invalid sampleRate {pcm.SampleRate}");
        }
        public static int GetBitRate(this AudioFormat.Types.PCM pcm)
        {
            var bitrate = 1;

            switch(pcm.ChannelLayout)
            {
                case AudioFormat.Types.ChannelLayout.AudioChannelLayoutStereo:
                    bitrate *= 2;
                    break;
            }

            switch(pcm.SampleFormat)
            {
                case AudioFormat.Types.SampleFormat.AudioSampleFormatAlaw:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatMulaw:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatU8:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatS8:
                    bitrate *= 8;
                    break;
                case AudioFormat.Types.SampleFormat.AudioSampleFormatS16Be:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatS16Le:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatU16Be:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatU16Le:
                    bitrate *= 16;
                    break;
                case AudioFormat.Types.SampleFormat.AudioSampleFormatS24Be:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatS24Le:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatU24Be:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatU24Le:
                    bitrate *= 24;
                    break;
                case AudioFormat.Types.SampleFormat.AudioSampleFormatF32Be:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatF32Le:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatS32Be:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatS32Le:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatU32Be:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatU32Le:
                    bitrate *= 32;
                    break;
                case AudioFormat.Types.SampleFormat.AudioSampleFormatF64Be:
                case AudioFormat.Types.SampleFormat.AudioSampleFormatF64Le:
                    bitrate *= 64;
                    break;
                default:
                    throw new Exception("Unsuported pcm sample format");
            }

            bitrate *= pcm.GetSampleRate();
            return bitrate;
            
        }

        public static string GetLabel(this cz.ntx.proto.v2t.engine.EngineContext engineContext)
        {
             
            bool withVAD = engineContext.ConfigCase== cz.ntx.proto.v2t.engine.EngineContext.ConfigOneofCase.Vad;
            bool withPPC = engineContext.ConfigCase== cz.ntx.proto.v2t.engine.EngineContext.ConfigOneofCase.Ppc;
            bool withV2T = engineContext.ConfigCase == cz.ntx.proto.v2t.engine.EngineContext.ConfigOneofCase.V2T;
            if (withV2T)
            {
                withVAD = engineContext.V2T.WithVAD != null;
                withPPC = engineContext.V2T.WithPPC != null;
            }
            //generate label for prometheus metrics
            List<string> _label = new List<string>();
            if (withVAD)
                _label.Add("vad");
            if (withV2T)
                _label.Add("v2t");
            if (withPPC)
                _label.Add("ppc");
            return string.Join("+", _label.ToArray());
        }

        public static cz.ntx.proto.v2t.engine.EngineContext ParseCmd(this cz.ntx.proto.v2t.engine.EngineContext engineContext, string label,string channel)
        {

            bool withVAD = false;
            bool withPPC = false;
            var module = label;
            withVAD = module.Contains("vad");
            withPPC = module.Contains("ppc");
            if (module.Contains("v2t"))
            {
                module = "v2t";
            }


            switch (module)
            {

                case "vad":
                    engineContext.Vad = new cz.ntx.proto.v2t.engine.EngineContext.Types.VADConfig();
                    break;
                case "v2t":
                    engineContext.V2T = new cz.ntx.proto.v2t.engine.EngineContext.Types.V2TConfig()
                    {
                        WithPPC = withPPC ? new cz.ntx.proto.v2t.engine.EngineContext.Types.PPCConfig() : null,
                        WithVAD = withVAD ? new cz.ntx.proto.v2t.engine.EngineContext.Types.VADConfig() : null,
                    };
                    break;
                case "ppc":
                    engineContext.Ppc = new cz.ntx.proto.v2t.engine.EngineContext.Types.PPCConfig();
                    break;
                default:
                    throw new Exception("Invalid module: " + module);
            }
            
            switch(channel)
            {
                case "downmix":
                    engineContext.AudioChannel = EngineContext.Types.AudioChannel.Downmix;
                    break;
                case "left":
                    engineContext.AudioChannel = EngineContext.Types.AudioChannel.Left;
                    break;
                case "right":
                    engineContext.AudioChannel = EngineContext.Types.AudioChannel.Right;
                    break;
                default:
                    throw new Exception($"Invalid audio channel option {channel}");
            }

            return engineContext;
        }

        public static cz.ntx.proto.v2t.misc.EventFilter ParseCmd(this cz.ntx.proto.v2t.misc.EventFilter filter, string cmd)
        {
            if(cmd.StartsWith("noise"))
            {
                string[] s = cmd.Split(':');
                if (s.Length < 3)
                    throw new Exception("Invalid noise filter definition: " + cmd);
                filter.Noise = new cz.ntx.proto.v2t.misc.EventFilter.Types.NoiseEventFilter
                {
                    Duration = (ulong)TimeSpan.FromSeconds(float.Parse(s[1])).Ticks,
                    ParagraphSymbol = Regex.Unescape(s[2])
                };
            return filter;
            }
            if(cmd == "none")
            {
                return new cz.ntx.proto.v2t.misc.EventFilter();
            }
            throw new Exception("Invalid filter format: " + cmd);
        }

        public static Func<MediaSource,MediaMapping> GetDefaultMediaMapper(Guid streamId,string programName,
            string language, string disposition, bool noVideo, bool isLive, uint audioBandwidth, uint videoBandwidth)
        {

           return new Func<MediaSource, MediaMapping>((mediaSource) =>
           {


               var ret = new MediaMapping();
               var program = mediaSource.Programs.First(x => Regex.IsMatch(x.Id, programName));
               ret.SrcProgramId = program.Id;
               //audio streams
               int id = 1;
               foreach (var s in program.Streams.Where(x => x.StreamsCase == MediaStream.StreamsOneofCase.Audio).Select(x => x.Audio))
               {
                   if (!s.Decodable)
                       continue;
                   if (Regex.IsMatch(s.Disposition, disposition) && Regex.IsMatch(s.Language, language))
                   {
                       var aset = new MediaAdaptation
                       {
                           Audio = new AudioMediaAdaptation
                           {
                               Language = s.Language,
                               Disposition = s.Disposition,
                               SrcStreamId = s.Id,
                               Id = string.Format("audio-{0}-{1}", s.Language, s.Disposition)
                           }
                       };

                       var repre = new AudioMediaRepresentation()
                       {
                           Bandwidth = audioBandwidth,
                           Codec = new AudioCodec() { Aac = new AudioCodec.Types.AAC() },
                           Description = "Audio AAC low profile",
                           Id = string.Format("audio-{0}bps", audioBandwidth)

                       };
                       aset.Audio.Representations.Add(repre);
                       ret.MediaAdaptations.Add(aset);
                       id++;
                   }
               }
               if (!noVideo && program.Streams.Count(x => x.StreamsCase == cz.ntx.proto.media.MediaStream.StreamsOneofCase.Video) > 0)
               {
                   var video = program.Streams.Where(x => x.StreamsCase == cz.ntx.proto.media.MediaStream.StreamsOneofCase.Video).Select(x => x.Video).OrderByDescending(i => i.Bitrate).First();
                   var aset = new MediaAdaptation
                   {
                       Video = new VideoMediaAdaptation
                       {
                           SrcStreamId = video.Id,
                           Id = "main-video"
                       }
                   };
                   var repre = new VideoMediaRepresentation()
                   {
                       Bandwidth = videoBandwidth,
                       Codec = new VideoCodec() { H264 = new VideoCodec.Types.H264() },
                       Description = "Video H264 baseline profile",
                       Id = string.Format("video-{0}bps", videoBandwidth),
                       Dar = video.Dar,
                       Sar = video.Sar,
                       Height = video.Height,
                       Width = video.Width,
                   };

                   aset.Video.Representations.Add(repre);
                   ret.MediaAdaptations.Add(aset);
               }

               ret.StreamId =streamId.ToString();
               ret.PeriodId = Guid.NewGuid().ToString();
               ret.PeriodOffset = new cz.ntx.proto.TimeOffsetFraction { };
               if (mediaSource.MediaTime != null)
               {
                   ret.PeriodOffset = mediaSource.MediaTime;
               }
               else if (isLive)
               {
                   if (mediaSource.StreamTime != null)
                   {
                       ret.PeriodOffset = mediaSource.StreamTime;
                   }
                   else
                   {
                       ret.PeriodOffset = mediaSource.RealTime;
                   }
               }
               return ret;
           });
        }
        
    }
}
