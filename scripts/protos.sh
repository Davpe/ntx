#!/bin/bash
cp -r scripts /scripts
dotnet restore /scripts/tools.csproj 
export GRPC_TOOLS_PATH=/root/.nuget/packages/grpc.tools/1.3.6/tools/linux_x64
perl /work/scripts/make_protobuf
chmod 666 -R /work/ntx.api/protos