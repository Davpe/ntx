﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using System.IO;
using ntx.api.io;
using ntx.api.pipe;
using ntx.api.pipe.tasks;
using System.Collections.Async;
using System.Threading.Tasks;

namespace ntx.command.devel
{
    class Command : ICommand
    {
        
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "devel and debug tools";
            command.HelpOption("-h|--help");
            command.ShowInHelpText = false;
            command.Command("run", (c) => run.Command.Configure(c, options));
            command.Command("task", (c) => task.Command.Configure(c, options));
            command.Command("nta", (c) => nta.Command.Configure(c, options));
            command.OnExecute(() =>
            {
                options.Command = new Command(command);
                return 0;
            });
        }

        private readonly CommandLineApplication _app;
        public Command(CommandLineApplication app)
        {
            _app = app;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            await _app.ShowHelpAsync();
            return 0;
        }
    }
}
