﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using System.IO;
using ntx.api.io;
using ntx.api.pipe;
using ntx.api.pipe.tasks;
using System.Threading.Tasks;
using ntx.api.auth;

namespace ntx.command.task.list
{
    class Command : ICommand
    {
        private string ServiceOption { get; set; }
        private string LabelOption { get; set; }
        private string[] TagsOption { get; set; }
        private CommandLineOptions GlobalOptions { get; set; }
        private string OutputUriOption { get; set; }
        private bool ExperimentalOption { get; set; }
        private string OutFormat { get; set; }
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "list tasks";
            command.HelpOption("-h|--help");
            var serviceOption = command.Option(@"-s|--service",
             "filter by service id",
             CommandOptionType.SingleValue
             );
            var labelOption = command.Option(@"-l|--label",
               "filter by task label",
               CommandOptionType.SingleValue
               );
            var tagsOption = command.Option(@"-t|--tags",
               "filter by tags",
               CommandOptionType.MultipleValue
               );
            var experimentalOption = command.Option(@"-e|--experimental",
              "list also experimental tasks",
              CommandOptionType.NoValue
              );
            var outputUriOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );
            var outFormat =  command.Option("-g|--oformat <txt>",
                "output format txt|json",
                CommandOptionType.SingleValue
                );
            command.OnExecute(() =>
            {
                options.Command = new Command(command)
                {
                    LabelOption = labelOption.HasValue() ? labelOption.Value() : null,
                    TagsOption = tagsOption.HasValue() ? tagsOption.Values.ToArray() : null,
                    OutputUriOption = outputUriOption.HasValue() ? outputUriOption.Value() : outputUriOption.ValueName,
                    ServiceOption = serviceOption.HasValue() ? serviceOption.Value() : null,
                    GlobalOptions = options,
                    ExperimentalOption = experimentalOption.HasValue(),
                    OutFormat = outFormat.GetValueOrDefault()
                };
                return 0;
            });
            
        }

        private readonly CommandLineApplication _app;
        public Command(CommandLineApplication app)
        {
            _app = app;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var outputResolver = LazyStream.Output(OutputUriOption,"text/plain",breaker);
            var tokenStream = LazyStream.Input(GlobalOptions.TaskAuthToken);
            var accessTokenFactory = Authorization.AccesTokenFactory(tokenStream);

            var pipe = PipeSource.TasksFromLocalStore(accessTokenFactory, breaker).ViaFilterAsync(ServiceOption, LabelOption, TagsOption);
            if(!ExperimentalOption)
            {
                pipe = pipe.ViaFilterAsync((x) => x.Tags.Contains("experimental") ? null : x);
            }

            switch(OutFormat)
            {
                case "txt":
                        await pipe.ToStringChunkStream()
                        .RunWithSink(
                            PipeSink.StringChunkStream(outputResolver)
                            );
                    break;
                case "json":
                        await pipe.RunWithSink(
                            PipeSink.ProtoJsonStream<cz.ntx.proto.task.store.StoreContent.Types.Task>(outputResolver)
                        );
                    break;
                default:
                    throw new Exception($"Invalid oformat {OutFormat}");
            }
            return 0;
        }
    }
}
