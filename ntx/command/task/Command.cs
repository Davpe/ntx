﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using System.Threading.Tasks;

namespace ntx.command.task
{
    class Command : ICommand
    {
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "run or list cloud tasks";
            command.HelpOption("-h|--help");
            command.Command("list", (c) => list.Command.Configure(c, options));
            command.Command("run", (c) => run.Command.Configure(c, options));
            command.Command("token", (c) => token.Command.Configure(c, options));

            command.OnExecute(() =>
            {
                options.Command = new Command(command);
                return 0;
            });
            
        }

        private readonly CommandLineApplication _app;
        public Command(CommandLineApplication app)
        {
            _app = app;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            await _app.ShowHelpAsync();
            return 0;
        }
    }
}
