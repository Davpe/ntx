﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using cz.ntx.proto.v2t.engine;
using ntx.api.pipe;
using ntx.api;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using ntx.api.pipe.events;

namespace ntx.command.util.events.tokenize
{
    class Command : ICommand
    {
        
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            
            command.Description = "a very simple tokenizer to make events from text";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes: text"
             + Environment.NewLine
             + "Produces: ntx.v2t.engine.Events"
             + Environment.NewLine + Environment.NewLine
             + "At first every line is split by regex and then plus items are identified via match regex."
             + Environment.NewLine 
             +"Every line of file is considered to be one Events message" 
             + Environment.NewLine
             + Environment.NewLine  
             + $"default split: {Constants.DefaultTextSplit}" + Environment.NewLine 
             +$"default match: {Constants.DefaultPlusMatch}" + Environment.NewLine;
            var inputUriOption = command.Option("-i|--input",
                "input url",
                CommandOptionType.SingleValue
                );
            var outputOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );
            var splitOption = command.Option($"-s|--split <default>",
                "split regex",
                CommandOptionType.SingleValue
                );

            var plusOption = command.Option(@"-p|--plus <default>",
                "match plus items",
                CommandOptionType.SingleValue
                );

            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                options.Command = new Command(command)
                {

                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption =outputOption.GetValueOrDefault(),
                    SplitOption = splitOption.GetValueOrDefault(),
                    PlusOption=plusOption.GetValueOrDefault()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private string SplitOption { get; set; }
        private string NewLineOption { get; set; }
        private string PlusOption { get; set; }

        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var inputResolver = LazyStream.Input(InputUriOption,breaker);
            var outputStream = LazyStream.Output(OutputUriOption,"application/json");
            var sink = PipeSink.ProtoJsonStream<Events>(outputStream);
            Regex split = new Regex(SplitOption =="default" ? Constants.DefaultTextSplit : SplitOption);
            Regex plus = new Regex(PlusOption == "default" ? Constants.DefaultPlusMatch : PlusOption);

            await PipeSource.EventsFromRawTextStream(inputResolver, breaker).ViaReLabelAndSplit(split,plus).RunWithSink(sink);
            
            return 0;
        }
    }
}