﻿using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using System.Threading.Tasks;

namespace ntx.command.util.events.eval
{
    
    class Command :ICommand
    {
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Command("one", (c) => one.Command.Configure(c,options));
            command.Command("summary", (c) => summary.Command.Configure(c, options));
            command.Command("conv", (c) => conv.Command.Configure(c, options));
            command.Command("nta", (c) => nta.Command.Configure(c, options));
            command.Description = "speech recognition performance evaluation tools";
            command.HelpOption("-h|--help");
            command.OnExecute(() =>
            {
                options.Command = new Command(command);

                return 0;
            });
            
        }
        private readonly CommandLineApplication _app;
        public Command(CommandLineApplication app)
        {
            _app = app;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            await _app.ShowHelpAsync();
            return 1;
        }


    }
}
