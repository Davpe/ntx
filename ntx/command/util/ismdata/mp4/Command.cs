﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using ntx.api.pipe;
using System.IO;
using ntx.api.pipe.ismdata;
using System.Threading.Tasks;

using cz.ntx.proto.media;

namespace ntx.command.util.ismdata.mp4
{
    class Command : ICommand
    {

        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "dumps ismp period to fragmented mp4";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes: ntx.media.ISMData"
             + Environment.NewLine
             + "Produces: fragmented mp4 stream";
            var inputUriOption = command.Option("-i|--input",
                "input url",
                CommandOptionType.SingleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );
            var streamSelectOption = command.Option("-s|--stream <>",
                "select period where stream id starts with",
                CommandOptionType.SingleValue
                );
            var periodSelectOption = command.Option("-p|--period <>",
                "select period where period id starts with",
                CommandOptionType.SingleValue
                );
            var adaptationSelectOption = command.Option("-a|--adaptation <>",
                "select period where adaptation id starts with",
                CommandOptionType.SingleValue
                );
            var representationSelectOption = command.Option("-r|--representation <>",
                "select period where representation id starts with",
                CommandOptionType.SingleValue
                );
            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    StreamId = streamSelectOption.GetValueOrDefault(),
                    PeriodId = periodSelectOption.GetValueOrDefault(),
                    AdaptationId= adaptationSelectOption.GetValueOrDefault(),
                    RepresentationId= representationSelectOption.GetValueOrDefault()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private string StreamId { get; set; }
        private string PeriodId { get; set; }
        private string AdaptationId { get; set; }
        private string RepresentationId { get; set; }

        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var inputResolver = LazyStream.Input(InputUriOption,breaker);
            var outputResolver = LazyStream.Output(OutputUriOption, "application/octet-stream",breaker);

            var ismpSource = PipeSource.FromProtoBinaryStream<ISMData>(inputResolver, breaker);
            var binarySink = PipeSink.BinaryChunkStream(outputResolver);

            await ismpSource.ViaSinglePeriodFilter(StreamId, PeriodId, AdaptationId, RepresentationId)
                .ViaMP4Chunker().RunWithSink(binarySink);



            return 0;
        }
    }
}