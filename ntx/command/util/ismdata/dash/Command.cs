﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using ntx.api.pipe;
using System.IO;
using System.Threading.Tasks;
using cz.ntx.proto.media;
using ntx.api.pipe.ismdata;
namespace ntx.command.util.ismdata.dash
{
    class Command : ICommand
    {

        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "dump ismdata stream to dash tar package";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes: ntx.media.ISMData"
             + Environment.NewLine
             + "Produces: tar stream";
            var inputUriOption = command.Option("-i|--input",
                "input url",
                CommandOptionType.SingleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );
            command.OnExecute(() =>
            {
                if (!inputUriOption.HasValue())
                {
                    command.Error.WriteLine($"Missing: {inputUriOption.Description}");
                    command.ShowHelp();
                    return 1;
                }
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var inputResolver = LazyStream.Input(InputUriOption,breaker);
            var outputResolver = LazyStream.Output(OutputUriOption, "application/x-tar",breaker);

            var ismpSource = PipeSource.FromProtoBinaryStream<ISMData>(inputResolver, breaker);
            var tarSink = PipeSink.DashTarStream(outputResolver);


            await ismpSource.ViaPlayListCollector().RunWithSink(tarSink);
            
            
            return 0;
        }
    }
}