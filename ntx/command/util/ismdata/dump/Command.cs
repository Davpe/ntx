﻿using System;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using ntx.api.io;
using ntx.api.pipe;
using System.IO;
using System.Threading.Tasks;
using cz.ntx.proto.media;
using ntx.api.pipe.ismdata;
namespace ntx.command.util.ismdata.dump
{
    class Command : ICommand
    {

        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Description = "dump ismdata stream to json";
            command.HelpOption("-h|--help");
            command.ExtendedHelpText =
             "Consumes: ntx.media.ISMData"
             + Environment.NewLine
             + "Produces: ntx.media.ISMData";
            var inputUriOption = command.Option("-i|--input",
                "input url",
                CommandOptionType.SingleValue
                );
            var outputUriOption = command.Option("-o|--output <->",
                "output url",
                CommandOptionType.SingleValue
                );
            var dumpOption= command.Option("-c|--chunks",
                "dump with chunks",
                CommandOptionType.NoValue
                );
            var dumpHeaderOption = command.Option("-h|--headers",
               "dump periods with headers",
               CommandOptionType.NoValue
               );
            command.OnExecute(() =>
            {
                inputUriOption.MustSetValue(command);
                
                options.Command = new Command(command)
                {
                    InputUriOption = inputUriOption.Value(),
                    OutputUriOption = outputUriOption.GetValueOrDefault(),
                    DumpChunks = dumpOption.HasValue(),
                    DumpHeaders= dumpHeaderOption.HasValue()
                };
                return 0;
            });
        }
        private CommandLineApplication command;
        private string InputUriOption { get; set; }
        private string OutputUriOption { get; set; }
        private bool DumpChunks { get; set; }
        private bool DumpHeaders { get; set; }
        public Command(CommandLineApplication command)
        {
            this.command = command;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            var inputResolver = LazyStream.Input(InputUriOption,breaker);
            var outputResolver = LazyStream.Output(OutputUriOption, "application/json",breaker);

            var ismpSource = PipeSource.FromProtoBinaryStream<ISMData>(inputResolver, breaker);
            var jsonSink = PipeSink.ProtoJsonStream<ISMData>(outputResolver);

            Func<ISMData, ISMData> filter = (x) => x.PayloadCase == ISMData.PayloadOneofCase.Chunks ? null : x;
            if (DumpChunks)
                filter = (x) => x;

            Func<ISMData, ISMData> removeHeaders = (x) => 
            {
                if(x.PayloadCase== ISMData.PayloadOneofCase.Period)
                {
                    var p = x.Period;
                    p.Header =Google.Protobuf.ByteString.Empty;
                    foreach(var a in p.Adaptations)
                    {
                        foreach(var r in a.Representations)
                        {
                            r.Header = Google.Protobuf.ByteString.Empty;
                        }
                    }
                }
                return x;
            };
            if (DumpHeaders)
                removeHeaders = (x) => x;

            await ismpSource.ViaPlayListCollector()
                .ViaInterceptor(filter)
                .ViaInterceptor(removeHeaders)
                .RunWithSink(jsonSink);
            
            return 0;
        }
    }
}