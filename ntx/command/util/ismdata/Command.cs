﻿using Microsoft.Extensions.CommandLineUtils;
using System.Threading;
using System.Threading.Tasks;

namespace ntx.command.util.ismdata
{
    
    class Command :ICommand
    {
        internal static void Configure(CommandLineApplication command, CommandLineOptions options)
        {
            command.Command("mp4", (c) => mp4.Command.Configure(c,options));
            command.Command("dump", (c) => dump.Command.Configure(c, options));
            command.Command("dash", (c) => dash.Command.Configure(c, options));
            command.Description = "cli tools consuming ntx.media.ISMData";
            command.OnExecute(() =>
            {
                options.Command = new Command(command);

                return 0;
            });
            
        }
        private readonly CommandLineApplication _app;
        public Command(CommandLineApplication app)
        {
            _app = app;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            await _app.ShowHelpAsync();
            return 1;
        }
    }
}
