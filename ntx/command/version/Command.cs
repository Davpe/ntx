﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Microsoft.Extensions.CommandLineUtils;
using System.Threading.Tasks;

namespace ntx.command.version
{ 
    class Command : ICommand
    {
        internal static void Configure(CommandLineApplication c, CommandLineOptions options)
        {
            options.Command = new Command(options.Version, c);
            c.Description = "print version";
        }
        private string version;
        private CommandLineApplication app;
        public Command(string version, CommandLineApplication app)
        {
            this.version = version;
            this.app = app;
        }
        public async Task<int> RunAsync(CancellationToken breaker)
        {
            await app.Out.WriteLineAsync(version);
            return 0;
        }
    }
}
